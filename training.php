<?php /* Template Name: Training */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("training-block",
		"/wp-content/uploads/2017/09/training-bg.png",
		"CME-Certified Education on Children’s Mental Health",
		"Find training that fits your schedule and enhances your ability to care for those you serve"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
<?php require_once('footer.php'); ?>