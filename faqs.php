<?php /* Template Name: Faqs */ ?>
<?php require_once('header.php'); ?>
<div class="disclmer">
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
	<div class="faq-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>