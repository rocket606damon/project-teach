<?php require_once('header.php'); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>

<section class="intro-block questions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 match-height purple">
				<div class="inner">
					<h3>Why is Good Mental Health Important for Your Child?</h3>
					<p>Good mental health lets young people live their best lives. It is a key component in a child's healthy development. It positively affects his or her ability to learn, interact with peers, and reach his or her full potential.</p>
				</div>
			</div>
			<div class="col-md-4 match-height green">
				<div class="inner">
					<h3>As a Parent,<br> How Can You Help?</h3>
					<p>We all have a hand in promoting good mental health. One key role for parents and family members is being aware of the warning signs for mental health concerns. It is important to identify any concerns as early as possible in children and adolescents.</p>
				</div>
			</div>
			<div class="col-md-4 match-height blue">
				<div class="inner">
					<h3>How Does Early Identification Help?</h3>
					<p>It guides parents to find and use helpful resources. It leads to clear dialogue with health care providers. And it allows young people to access the care they need for any mental health concerns. All these steps help ensure that children have the best opportunities to lead full and productive lives.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="component-positive-impacts">
	<div class="container">
	  <div class="row">
	    <h2>Positive Impacts</h2>
	    <p>Social and emotional development is a primary benefit of good mental health. This affects every part of a child's life. Children who develop good social and emotional skills are able to: </p>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="service-txt">
          <!-- <div class="one number">1</div> -->
          <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/blue-icon-100-01.png" />
          <p>Make and Keep Friends</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="service-txt">
          <!-- <div class="two number">2</div> -->
          <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/orange-icon-100-01.png" />
          <p>Understand and Express Emotions</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="service-txt">
          <!-- <div class="tree number">3</div> -->
          <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/green-icon-100-01.png" />
          <p>Achieve Success in School</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="service-txt end">
          <!-- <div class="for number">4</div> -->
          <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/purple-icon-100-01.png" />
          <p>Develop Healthy Eating and Sleeping Patterns</p>
        </div>
      </div>
	  </div>
	</div>
</section>

<section class="intro-block prevention-is-important">
	<div class="container">
		<div class="row">
			<div class="col-md-6 match-height block-content">
				<h2>Prevention is Important</h2>
				<p>It is important to prevent negative medical, social, and emotional impacts before they occur.</p>
				<p>Factors involved in prevention include proper nutrition, social supports, family stability, and much more. Data shows that these factors can have large and long-term effects on health outcomes.</p>
				<a class="btn" href="http://project-teach.launchpaddev.com/prevention-science/" target="_blank">Click to Learn More</a>
			</div>
			<div class="col-md-6 match-height block-image">
				<img class="img-responsive visible-sm visible-xs" src="<?php echo get_template_directory_uri(); ?>/images/ps-what-is-prevention-science-nobg.png" />
			</div>
		</div>
	</div>
</section>

<section class="resources">
	<div class="container">
		<div class="row">

			<div class="top col-md-12">
				<h2>Resources for Parents and Family Members</h2>
				<p>Resources for Parents and Family Members Access these videos and flyers to help you identify and navigate any mental health concerns.</p>
			</div>

			<div class="video-blocks col-md-8">
				<div class="row">
					<div class="video col-sm-4">
						<a href="#" class="video-link" data-toggle="modal" data-target="#featured_video_a"><img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/signsandsymptoms_hqdefault_brighter.jpg" /></a>
						<p class="video-title">What to Discuss During Your Child’s Primary Care Visit</p>
					</div>
					<div class="video col-sm-4">
						<a href="#" class="video-link" data-toggle="modal" data-target="#featured_video_b"><img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/mentalwellness_hqdefault_brighter_newer.jpg" /></a>
						<p class="video-title">Mental Wellness in Children Ages 0-5</p>
					</div>
					<div class="video col-sm-4">
						<a href="#" class="video-link" data-toggle="modal" data-target="#featured_video_c"><img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/mentalwellness6_12_hqdefault.jpg" /></a>
						<p class="video-title">Mental Wellness in School- Age Children</p>
					</div>
				</div>
				<div class="row">
					<div class="view-all col-sm-12">
						<a class="view" href="http://project-teach.launchpaddev.com/resources/#tabs-1" target="_blank">View All Videos</a>
					</div>
				</div>
			</div>

			<div class="pdf-blocks col-md-4">
				<div class="row">
					<div class="pdf col-sm-12">
						<div class="pdf-resource cf">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
							<span class="inside">What to Discuss<br>During Your Child's<br>Primary Care Visit</span>
							<a class="inner" href="<?php bloginfo('template_directory'); ?>/images/Parents_What_to_Discuss_During_Primary_Care_Visit.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
						</div>
						<div class="pdf-resource cf">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
							<span class="inside">Mental Wellness <br>in Children Ages 0-5</span>
							<a class="inner" href="<?php bloginfo('template_directory'); ?>/images/Mental_Wellness_in_Children_Ages_0-5.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
						</div>
						<div class="pdf-resource cf">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
							<span class="inside">Mental Wellness <br>in School- Age Children</span>
							<a class="inner" href="<?php bloginfo('template_directory'); ?>/images/Mental_Wellness_in_School_Age_Children.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
						</div>
						
						<a class="view" href="http://project-teach.launchpaddev.com/resources/#tabs-3" target="_blank">View All Resources</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="learn-more-prevention-science">
	<div class="container">
		<h2>Where Can I Learn More?</h2>
		<div class="row">
			<div class="col-md-12">

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="false">Tools to Promote Wellness</a>
				      </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapsed collapse">
				      <div class="panel-body">
				      	<div class="item">
				      	  <div class="link">
				      	    <a href="https://omh.ny.gov/omhweb/resources/publications/" target="_blank">Parent Guides on Social and Emotional Wellness</a>
				      	  </div>
				      	  <p>A set of parent guides developed by the NYS Office of Mental Health to help parents better understand their child's social emotional development from ages 1 - 18.</p>
				      	  <ul class="omh-resources">
				      	  	<li><a href="https://omh.ny.gov/omhweb/childservice/docs/1-5-years-brochure.pdf" target="_blank">A Guide for Parents of One to Five Years Olds</a></li>
				      	  	<li><a href="https://omh.ny.gov/omhweb/childservice/docs/5-10-years-brochure.pdf" target="_blank">A Guide for Parents of Five to Ten Years Olds</a></li>
				      	  	<li><a href="https://omh.ny.gov/omhweb/childservice/docs/10-12-years-brochure.pdf" target="_blank">A Guide for Parents of Ten to Twelve Year Olds</a></li>
				      	  	<li><a href="https://omh.ny.gov/omhweb/childservice/docs/12-15-years-brochure.pdf" target="_blank">A Guide for Parents of Twelve to Fifteen Year Olds</a></li>
				      	  	<li><a href="https://omh.ny.gov/omhweb/childservice/docs/15-18-years-brochure.pdf" target="_blank">A Guide for Parents of Fifteen to Eighteen Year Olds</a></li>
				      	  </ul>
				      	</div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.mghclaycenter.org/" target="_blank">The Clay Center for Young Healthy Minds</a>
				          </div>
				          <p>A practical, online educational resource dedicated to promoting and supporting the mental, emotional, and behavioral well-being of children, teens, and young adults through a narrative, multimedia approach.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://brightfutures.aap.org/families/Pages/Resources-for-Families.aspx" target="_blank">Bright Futures</a>
				          </div>
				          <p>An American Academy of Pediatrics program that provides a common framework for well-child care from birth to age 21</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.healthychildren.org/English/Pages/default.aspx" target="_blank">AAP Healthy Children</a>
				          </div>
				          <p>An American Academy of Pediatrics program, parents with guidance on parenting issues to promote the optimal physical, mental, and social health for children.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.aacap.org/AACAP/Families_and_Youth/Facts_for_Families/Home.aspx" target="_blank">American Academy of Child and Adolescent Psychiatry (AACAP) Facts for Families</a>
				          </div>
				          <p>"Facts for Families" provide concise and up-to-date information on behavioral health issues that affect children, teenagers, and their families.</p>
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">Helpful Resources for a Variety of Mental Health Issues</a>
				      </h4>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapsed collapse">
				      <div class="panel-body">
				        <div class="item">
				          <div class="link">
				            <a href="http://ccf.ny.gov/" target="_blank">Council on Children and Families</a>
				          </div>
				          <p>The Council on Children and Families coordinates New York's health, education and human services systems to provide more effective systems of care for children and families.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.msnavigator.org/resources/resource_category_details?catID=9" target="_blank">NYS Multiple Systems Navigator - Resources for Family/Youth Support and Peer Advocacy</a>
				          </div>
				          <p>Provides resources from across multiple New York State systems to help young people, parents, and family members with behavioral, emotional, physical and/or mental health challenges.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://helpmegrowny.org/" target="_blank">Help Me Grow Western NY</a>
				          </div>
				          <p>A resource connecting families of children 0-5 years to community resources and child development information in Western NY and Long Island.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.aacap.org/AACAP/Families_and_Youth/Resource_Centers/Home.aspx?hkey=d603a11b-e1fb-489c-9e91-71d3c29757e2" target="_blank">AACAP Resource Centers</a>
				          </div>
				          <p>AACAP resources centers offer information regarding various mental health conditions, with definitions, answers to frequently asked questions, clinical resources, and expert videos.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.aacap.org/App_Themes/AACAP/Docs/resource_centers/adhd/adhd_parents_medication_guide_201305.pdf" target="_blank">AACAP ADHD Parents Medication Guide</a>
				          </div>
				          <p>An educational guide for families with children with ADHD.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://adaa.org/" target="_blank">Anxiety and Depression Association of America (ADAA)</a>
				          </div>
				          <p>An international nonprofit organization dedicated to the prevention, treatment, and cure of anxiety, depressive, obsessive-compulsive, and trauma-related disorders through education, practice, and research.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.understood.org/en" target="_blank">Understood</a>
				          </div>
				          <p>For parents whose children, between ages 3-20, are struggling with learning and attention issues.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.dbsalliance.org/site/PageServer?pagename=home" target="_blank">Depression and Bipolar Support Alliance (DBSA)</a>
				          </div>
				          <p>The leading peer-directed national organization focusing on the two most prevalent mental health conditions, depression and bipolar disorder.</p>
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseThree" aria-expanded="false">Peer Support Resources</a>
				      </h4>
				    </div>
				    <div id="collapseThree" class="panel-collapse collapsed collapse">
				      <div class="panel-body">
				        <div class="item">
				          <div class="link">
				            <a href="http://www.youthpower.org/" target="_blank">Youth Power</a>
				          </div>
				          <p>A USAID project, Youth Power seeks to improve the capacity of youth-led and youth-serving institutions through evidence-based positive youth development.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.ftnys.org/" target="_blank">Families Together in New York State</a>
				          </div>
				          <p>A statewise, parent-governed, non-profit organization representing families of children with social, emotional, behavioral and cross-system challenges.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.nami.org/" target="_blank">National Alliance on Mental Illness</a>
				          </div>
				          <p>The nation's largest grassroots mental health organization aiming to raise awareness and provide support and education.</p>
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseFour" aria-expanded="false">How to Have Conversations About Mental Health Issues, Substance Use and Suicide</a>
				      </h4>
				    </div>
				    <div id="collapseFour" class="panel-collapse collapsed collapse">
				      <div class="panel-body">
				        <div class="item">
				          <div class="link">
				            <a href="http://www.sesamestreet.org/toolkits" target="_blank">Sesame Street Workshop Parent's Guide to Helping Children Cope with Stress - Toolkits</a>
				          </div>
				          <p>A selection of toolkits and guides for parents to address behavioral health and wellness issues.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.sesamestreet.org/toolkits/stages" target="_blank">Ages and Stages</a>
				          </div>
				          <p>These resources are designed to support children and families in reaching certain milestones at different ages and stages.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://autism.sesamestreet.org/" target="_blank">Autism</a>
				          </div>
				          <p>A nationwise initiative aimed at communities with children ages 2 to 5, offering families ways to manage common challenges, to simplify everyday activities, and to grow connections.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.sesamestreet.org/toolkits/talking" target="_blank">Brain Development</a>
				          </div>
				          <p>Tips for helping children to develop age-appropriate cognitive and motor skills.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.sesamestreet.org/toolkits/challenges" target="_blank">Resilience</a>
				          </div>
				          <p>Tips for parents and educators on helping children develop the self-expression and self-confidence to manage challenges.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://www.drugcocktails.ca/" target="_blank">Drugcocktails.ca</a>
				          </div>
				          <p>Website for teens, parents, professionals that includes information on drugs of abuse and their interactions with medications.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.youtube.com/watch?v=gdc4WDG99fg" target="_blank">Mayo Clinic - "Not My Kid: What Parents Should Know About Teen Suicide" Video</a>
				          </div>
				          <p>Teens describe common signs of suicidality, and provide tips for communication and directing to support and safety.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://www.psychiatry.org/news-room/apa-blogs/apa-blog/2017/04/13-mental-health-questions-about-13-reasons-why" target="_blank">American Psychiatric Association - "13 Mental Health Questions About 13 Reasons Why" Article</a>
				          </div>
				          <p>The APA offers answers to common questions and concerns regarding bullying, depression, trauma and suicide brought up by the Netflix show, "13 Reasons Why."</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="https://afsp.org/about-suicide/" target="_blank">American Foundation for Suicide Prevention - Mental Health for Parents Having conversations with Young Adults</a>
				          </div>
				          <p>Videos, guides, and other resources for parents.</p>
				        </div>
				        <div class="item">
				          <div class="link">
				            <a href="http://13reasonswhy.info/assets/pdf/13RW Talking Points Final.pdf" target="_blank">The Jed Foundation and Suicide Awareness Voices of Education - "13 Reasons Why Talking Points"</a>
				          </div>
				          <p>Guidance on relating to the show the Netflix show, "13 Reasons Why" and issues of suicidality.</p>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>

			</div>
		</div>
	</div>
</section>

<div class="modal" id="featured_video_a" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <h3 class="modal-title" id="myModalLabel">Video</h3>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/w_K9C5DhKHk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="featured_video_b" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <h3 class="modal-title" id="myModalLabel">Video</h3>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/QuD70c0nmb8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="featured_video_c" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <h3 class="modal-title" id="myModalLabel">Video</h3>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/8GM4KFRtJmY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('footer.php'); ?>