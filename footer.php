<!-- footer section codes starts here-->
		 <footer>
				<div class="footer-main">
			<div class="container">
					 <div class="row">
							 <div class="col-md-4 col-sm-4 col-xs-12 left-sec">
									 <div class="ftr-logo">
											 <a href="/home/" class="ftr-logo" title="Project TEACH"><img src="<?php echo get_pt_attachment_url('2017/09/footer-logo.png'); ?>" alt="Project TEACH" /></a>
									 </div>
									 <div class="health-logo">
											<span>A Project Funded by</span>
											 <a href="https://www.omh.ny.gov/" target="_blank" title="Office of Mental Health">
												 <img src="<?php echo get_pt_attachment_url('2017/09/mental-health-logo.png'); ?>" alt="mental helth" />
											 </a>
									 </div>
							 </div>
							 <div class="col-md-5 col-sm-5 col-xs-12 ftr-project_teach">
									 <h6>ABOUT PROJECT TEACH</h6>
									 <p>Our mission is to strengthen and support the ability of New York's pediatric primary care providers (PCPs) to deliver care to children and families who experience mild-to-moderate mental health concerns.</p>
									 <ul class="about_menu">
											 <li><a href="<?php echo get_link_by_slug("accessibility"); ?>" title="ACCESSIBILITY">ACCESSIBILITY</a></li>
											 <li><a href="<?php echo get_link_by_slug("disclaimer"); ?>" title="DISCLAIMER">DISCLAIMER</a></li>
											 <li><a href="<?php echo get_link_by_slug("privacy-policy"); ?>" title="PRIVACY">PRIVACY</a></li>
											 <li><a href="<?php echo get_link_by_slug("site-map"); ?>" title="SITE MAP">SITE MAP</a></li>
											 <li><a href="<?php echo get_link_by_slug("/faqs"); ?>" title="FAQs">FAQS</a></li>
									 </ul>
									 <span>Copyright &copy; 2018 Project TEACH. All Rights Reserved.</span>
							 </div>
							 <div class="col-md-3 col-sm-3 col-xs-12 contact_text">
									 <h6>CONTACT US</h6>
									 <ul class="social_media">
											 <li><span><img src="<?php echo get_pt_attachment_url('2017/09/phone-icoon.png'); ?>" alt="phone" /></span>
												 <a href="tel:8777091771" title="Call Us">(877) 709-1771</a></li>
											 <li><span><img src="<?php echo get_pt_attachment_url('2017/09/massege-icon.png'); ?>" alt="massege" /></span>
												 <a href="mailto:info@projectteachny.org" title="Mail Us">info@projectteachny.org</a></li>
											 <li><span><img src="<?php echo get_pt_attachment_url('2017/09//watch-icon.png'); ?>" alt="watchicon" /></span>
												 Hours: 9:30 AM to 4:30 PM</li>
									 </ul>
							 </div>
					 </div>
					 <div class="ftr-copy-txt"></div>
			</div>
		</div>
			</footer>
<!-- footer section codes ends here-->

</div>
<!--footer starts here-->

<!-- image preloads starts here-->
<div class="preloader"></div>
<!-- image preloads ends here-->
<!--footer ends here-->

<?php
if(is_page('intensive-training')){
	include('google-map.php');
} elseif(is_page('region-1')){
	include('google-map/google-map-1.php');
} elseif(is_page('region-2')){
	include('google-map/google-map-2.php');
} elseif(is_page('region-3')){
	include('google-map/google-map-3.php');
} elseif(is_page('annual-forum')){
	include('google-map/google-map-4.php');
} elseif (is_page(947)) {
	include('google-map/google-map-5.php');
}

?>
<!--scripts starts here-->
<script>
function googleTranslateElementInit() {
	new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script  src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.7.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight-min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<?php if(is_page(array('test', 'mmh', 'reserve-a-time'))): ?>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-includes/js/comment-reply.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/js/libs/jquery.validate.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/js/libs/jquery-ui-i18n.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/js/libs/moment.min.js?ver=4.9.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ea_settings = {"mail.pending":"pending","mail.reservation":"reservation","mail.canceled":"canceled","mail.confirmed":"confirmed","mail.admin":"","trans.service":"Service","trans.location":"Location","trans.worker":"Worker","trans.done_message":"Done","time_format":"h:mm a","trans.currency":"$","pending.email":"nikolanbg@gmail.com","price.hide":"0","datepicker":"en-US","send.user.email":"0","custom.css":"body .site-header{padding-top:0}body .entry-content .calendar a{box-shadow:0 0}#booking-overview table{width:100%;border-left:1px dotted #ccc;border-right:1px dotted #ccc;border-bottom:1px dotted #ccc}#booking-overview table td{padding:5px;border-top:1px dotted #ccc}.ea-standard.ea-standard .step input{margin-top:5px;width:63%}.ea-standard.ea-standard .step label{font-weight:400;width:37%}.ea-standard.ea-standard .step select{width:63%;margin-top:5px;display:inline-block} #ui-datepicker-div{display:none}.ea-bootstrap.ea-bootstrap .disabled .block { z-index: 101}","show.iagree":"1","cancel.scroll":"calendar","multiple.work":"1","compatibility.mode":"0","pending.subject.email":"New Reservation #date# #id#","send.from.email":"","css.off":"0","submit.redirect":"","pending.subject.visitor.email":"Reservation #id#","block.time":"0","max.appointments":"5","pre.reservation":"0","default.status":"pending","send.worker.email":"0","currency.before":"0","nonce.off":"0","gdpr.on":"0","gdpr.label":"By using this form you agree with the storage and handling of your data by this website.","gdpr.link":"","gdpr.message":"You need to accept the privacy checkbox","sort.workers-by":"id","sort.services-by":"id","sort.locations-by":"id","order.workers-by":"DESC","order.services-by":"DESC","order.locations-by":"DESC","check":"cb2c5cf1bb","scroll_off":"","start_of_week":"1","default_date":"2018-10-10","min_date":null,"max_date":null,"show_remaining_slots":"0","save_form_content":"1","trans.please-select-new-date":"Please select another day","trans.date-time":"Date & time","trans.price":"Price","date_format":"MMMM D, YYYY","default_datetime_format":"YYYY-MM-DD HH:mm","trans.nonce-expired":"Form validation code expired. Please refresh page in order to continue.","trans.internal-error":"Internal error. Please try again later.","trans.ajax-call-not-available":"Unable to make ajax request. Please try again later."};
var ea_settings = {"mail.pending":"pending","mail.reservation":"reservation","mail.canceled":"canceled","mail.confirmed":"confirmed","mail.admin":"","trans.service":"Service","trans.location":"Location","trans.worker":"Worker","trans.done_message":"Done","time_format":"h:mm a","trans.currency":"$","pending.email":"nikolanbg@gmail.com","price.hide":"0","datepicker":"en-US","send.user.email":"0","custom.css":"body .site-header{padding-top:0}body .entry-content .calendar a{box-shadow:0 0}#booking-overview table{width:100%;border-left:1px dotted #ccc;border-right:1px dotted #ccc;border-bottom:1px dotted #ccc}#booking-overview table td{padding:5px;border-top:1px dotted #ccc}.ea-standard.ea-standard .step input{margin-top:5px;width:63%}.ea-standard.ea-standard .step label{font-weight:400;width:37%}.ea-standard.ea-standard .step select{width:63%;margin-top:5px;display:inline-block} #ui-datepicker-div{display:none}.ea-bootstrap.ea-bootstrap .disabled .block { z-index: 101}","show.iagree":"1","cancel.scroll":"calendar","multiple.work":"1","compatibility.mode":"0","pending.subject.email":"New Reservation #date# #id#","send.from.email":"","css.off":"0","submit.redirect":"","pending.subject.visitor.email":"Reservation #id#","block.time":"0","max.appointments":"5","pre.reservation":"0","default.status":"pending","send.worker.email":"0","currency.before":"0","nonce.off":"0","gdpr.on":"0","gdpr.label":"By using this form you agree with the storage and handling of your data by this website.","gdpr.link":"","gdpr.message":"You need to accept the privacy checkbox","sort.workers-by":"id","sort.services-by":"id","sort.locations-by":"id","order.workers-by":"DESC","order.services-by":"DESC","order.locations-by":"DESC","check":"cb2c5cf1bb","scroll_off":"","start_of_week":"1","default_date":"2018-10-10","min_date":null,"max_date":null,"show_remaining_slots":"0","save_form_content":"1","trans.please-select-new-date":"Please select another day","trans.date-time":"Date & time","trans.price":"Price","date_format":"MMMM D, YYYY","default_datetime_format":"YYYY-MM-DD HH:mm","trans.nonce-expired":"Form validation code expired. Please refresh page in order to continue.","trans.internal-error":"Internal error. Please try again later.","trans.ajax-call-not-available":"Unable to make ajax request. Please try again later."};
var ea_settings = {"mail.pending":"pending","mail.reservation":"reservation","mail.canceled":"canceled","mail.confirmed":"confirmed","mail.admin":"","trans.service":"Service","trans.location":"Location","trans.worker":"Worker","trans.done_message":"Done","time_format":"h:mm a","trans.currency":"$","pending.email":"nikolanbg@gmail.com","price.hide":"0","datepicker":"en-US","send.user.email":"0","custom.css":"body .site-header{padding-top:0}body .entry-content .calendar a{box-shadow:0 0}#booking-overview table{width:100%;border-left:1px dotted #ccc;border-right:1px dotted #ccc;border-bottom:1px dotted #ccc}#booking-overview table td{padding:5px;border-top:1px dotted #ccc}.ea-standard.ea-standard .step input{margin-top:5px;width:63%}.ea-standard.ea-standard .step label{font-weight:400;width:37%}.ea-standard.ea-standard .step select{width:63%;margin-top:5px;display:inline-block} #ui-datepicker-div{display:none}.ea-bootstrap.ea-bootstrap .disabled .block { z-index: 101}","show.iagree":"1","cancel.scroll":"calendar","multiple.work":"1","compatibility.mode":"0","pending.subject.email":"New Reservation #date# #id#","send.from.email":"","css.off":"0","submit.redirect":"","pending.subject.visitor.email":"Reservation #id#","block.time":"0","max.appointments":"5","pre.reservation":"0","default.status":"pending","send.worker.email":"0","currency.before":"0","nonce.off":"0","gdpr.on":"0","gdpr.label":"By using this form you agree with the storage and handling of your data by this website.","gdpr.link":"","gdpr.message":"You need to accept the privacy checkbox","sort.workers-by":"id","sort.services-by":"id","sort.locations-by":"id","order.workers-by":"DESC","order.services-by":"DESC","order.locations-by":"DESC","check":"cb2c5cf1bb","scroll_off":"","start_of_week":"1","default_date":"2018-10-10","min_date":null,"max_date":null,"show_remaining_slots":"0","save_form_content":"1","trans.please-select-new-date":"Please select another day","trans.date-time":"Date & time","trans.price":"Price","date_format":"MMMM D, YYYY","default_datetime_format":"YYYY-MM-DD HH:mm","trans.nonce-expired":"Form validation code expired. Please refresh page in order to continue.","trans.internal-error":"Internal error. Please try again later.","trans.ajax-call-not-available":"Unable to make ajax request. Please try again later."};
/* ]]> */
</script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/js/frontend.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://easy-appointments.net/wordpress/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
<script>
    /**
     * Add to chart button event
     */
    jQuery(document).ready(function(){
        jQuery('.btn-add-cart').on('click', function() {
            var $e = jQuery(this);
            $e.parent().parent().find('.wp-cart-button-form').submit();
        });
    });
</script>
<?php endif; ?> 
<script src="<?php echo get_template_directory_uri(); ?>/js/general.js?<?php echo filemtime('/home/projec66/public_html/wp-content/themes/project-teach/js/general.js'); ?>"></script>
<?php if(is_front_page() | is_page(array('consultation', 'referrals'))): ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/cookie.js"></script>
<?php endif; ?>
<!--scripts end here-->

<?php get_footer(); ?>

</body>
</html>
