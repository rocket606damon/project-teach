<?php /* Template Name: Get Involved */ ?>
<?php require_once('header.php'); ?>
  <div class="get-involved-block">
  <div class="banner" style="background-image: url(/wp-content/uploads/2017/09/get-involved-bg.png)">
      <div class="container">
        <div class="row">
          <div class="banner-text col-md-8 col-sm-12 col-xs-12">
            <h1>Better Health.<br> Brighter Future.</h1>
            <p>Get involved today &mdash; it's easy! Sign up your primary care office for our services.</p>
          </div>
        </div>
      </div>
  </div>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
</div>
<?php require_once('footer.php'); ?>