<?php /* Template Name: About Template */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("about_block",
		"/wp-content/uploads/2017/09/about-bg.png",
		"Better Health.<br> Brighter Future.",
		"Learn more about our vision for a healthy New York State"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php
while ( have_posts() ) : the_post();
	the_content();
endwhile;
?>
</div>
<?php require_once('footer.php'); ?>