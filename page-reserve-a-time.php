<?php require_once('header.php'); ?>
<div class="privacy-plcy">
<?php
remove_filter( 'the_content', 'wpautop' );

//remove_filter( 'the_excerpt', 'wpautop' );
?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
	<div class="privacy-policy-content">
		<div class="container">
			<div class="row">
				<?php while ( have_posts() ) : the_post(); ?>
          <?php //the_content(); ?>
				  <?php echo do_shortcode("[ea_standard]"); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>
