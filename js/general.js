var $ = jQuery.noConflict();

$(document).ready(function () {

    $('.match-height').matchHeight();
    
	 headerOuterHeight_ = $(".header-main").outerHeight();
    var Obj_ = $("<div>", {
        class: "blankDiv"
    }).height(headerOuterHeight_).hide().insertAfter($(".header-main"));

    stickyHeader();

    var topMenu = jQuery("header"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find('a[href*="#"]'),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function () {
            var href = jQuery(this).attr("href"),
                    id = href.substring(href.indexOf('#')),
                    item = jQuery(id);
            //console.log(item)
            if (item.length) {
                return item;
            }
        });


    // Bind to scroll
    jQuery(window).scroll(function () {
        // Get container scroll position
        var fromTop = jQuery(this).scrollTop() + topMenuHeight + 2;

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if (jQuery(this).offset().top < fromTop)
                return this;
        });

        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        menuItems.parent().removeClass("active");
        if (id) {
            menuItems.parent().end().filter("[href*='#" + id + "']").parent().addClass("active");
        }

    });

    $('.slider-event').owlCarousel({
        margin: 40,
        loop: true,
        items: 3,
        stagePadding: 100,
        nav: true,
        responsive: {
            
            0: {
               items: 1,
               stagePadding: 0,
            },
            
            768: {
                items: 1,
            },
            990: {
                items: 1,
            },
            1200: {
                items: 2,
            },
            1600:{
                items: 3,
            },
            1920:{
                items: 3,
            },
            
            
            
        }
    });

    /**** equalHeight starts ****/
    $(".consulations").equalHeight();
    $(".primary-txt figcaption").equalHeight();
    $(".box-height").equalHeight();
    $(".box-height1").equalHeight();
    $(".overview-info p").equalHeight();
    /**** equalHeight end ****/

    /**** Init Modal ****/
    $('#welcome-modal').modal('show');

    /**** Top Navigation ****/


    });

;(function($){
    $.fn.equalHeight = function (option) {
        var $this = this
        var get_height = function(){
            var maxheight=0;
            $this.css("height","")
            $this.each(function(){
                maxheight = $(this).height() > maxheight ? $(this).height() : maxheight;
            })
            $this.height(maxheight)
        }
        var init =function(){
            get_height()
            $(window).bind("resize",get_height)
        }
        $this.destroy = function(){
            $this.css("height","")
            $(window).unbind("resize",get_height)
        }
        init()
        return this
    }
})(jQuery)


/****** sticky header starts here ******/
$(window).scroll(function () {
	//activeNav();
	stickyHeader();
});
$(window).load(function () {
	//activeNav();
	stickyHeader();
    $(".box-height").equalHeight();
    $(".box-height1").equalHeight();
});

$(window).resize(function () {

    headerOuterHeight_ = $(".header-main").outerHeight();
    $(".blankDiv").height(headerOuterHeight_)
    stickyHeader();
    $(".box-height").equalHeight();
    $(".box-height1").equalHeight();
    $(".overview-info p").equalHeight();
});



function stickyHeader() {
    if ($(window).scrollTop() > headerOuterHeight_) {
        $("header").addClass("sticky");
        //$(".blankDiv").show();
    } else {
        $("header").removeClass("sticky");
        //$(".blankDiv").hide();
    }
}
/****** sticky header ends here ******/
$(function () {
    $('a[href*="#"]:not([href="#"], [data-toggle=collapse])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if ($(".menu-icon").is(":visible") && $(".menu-icon").hasClass("active")) {
                $(".menu-icon").trigger("click");
            }
            if (target.length) {
                $('.enumenu_ul li').removeClass("active");
                $(this).parent().addClass("active");
                $('html, body').stop().animate({
                    scrollTop: target.offset().top - $("header").outerHeight(true)
                }, 1000, function () {
                    var t = target.offset().top
                    if (t != $(window).scrollTop()) {
                        $('html, body').stop().animate({
                            scrollTop: target.offset().top - $("header").outerHeight(true)
                        }, 100);
                    }
                });
                return false;
            }
        }
    });
});

/****** svg fxn ******/

$(function () {
    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');


            $img.replaceWith($svg);

        }, 'xml');

    });
});

