<?php /* Template Name: Clinical Rating Scales */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("resources",
		"/2017/09/regional-providers-bg.png",
		"Resources:<br> Clinical Rating Scales"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
 <?php require_once('footer.php'); ?>
