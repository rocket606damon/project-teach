<?php get_header(); ?>
<?php renderBanner("about_block",
		"2017/09/about-bg.png",
		"CME-Certified Education on Children's Mental Health",
		"Find training that fits your schedule and enhances your ability to care for those you serve"); ?>

	<section class="single-post">
	<div class="container-fluid container">
	<div class="row">
	<main class="main cf" role="main">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="header">
				<div class="row">
					<?php 
						$user_id = get_the_author_meta('ID');
	          $user_name =  get_the_author_meta('display_name');
	          $description = get_the_author_meta('description');
	          $size = 'thumbnail';
	          $user_photo = get_cupp_meta($user_id, $size);
					?>
					<div class="col-xs-12">
						<?php if($user_photo): ?>
	          <img class="author-thumb img-circle" src="<?php echo $user_photo; ?>" alt="<?php echo $user_name; ?>">
	          <?php endif; ?>
	          <h3 class="entry-title"><?php the_title(); ?></h3>
	          <p><time class="updated"><?php the_time('F j, Y'); ?></time> <span class="byline author vcard"><?php _e( 'by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span></p>
					</div>

				</div>
			</div>

			<div class="entry-content">
			<?php the_content(); // Dynamic Content ?>

			<p><?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></p>

			<p><?php _e( 'Categories: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

			<?php //comments_template(); ?>
			</div>


		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</main>

	<aside class="sidebar" role="complementary">
		<?php dynamic_sidebar('primary'); ?>
	</aside>
	
	</div>
	</div>
</section>

<div class="cf"></div>

<?php get_footer(); ?>
