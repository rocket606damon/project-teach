<?php require_once('header.php'); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>

<section class="intro-block what-is-prevention-science">
	<div class="container">
		<div class="row">
			<div class="col-md-6 match-height block-content">
				<h2>What is Prevention Science?</h2>
				<p>Prevention Science is a framework for research on how to prevent and/or moderate negative medical, social, and emotional impacts before they occur. Over the past four decades, the science of prevention in medicine has grown steadily.</p>
				<p>Pediatricians have helped drive the higher use of screening and intervention practices in primary care. Common practices – such as immunizations and well-child visits – are now enhanced by routine screening and guidance. The goal of this is to prevent a wide range of developmental, behavioral and social problems.</p>
			</div>
			<div class="col-md-6 match-height block-image">
				<img class="img-responsive visible-sm visible-xs" src="<?php echo get_template_directory_uri(); ?>/images/ps-what-is-prevention-science-nobg.png" />
			</div>
		</div>
	</div>
</section>

<section class="intro-block why-prevention-science-important">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-push-6 match-height block-content">
				<h2>Why is Prevention Science so Important?</h2>
				<p>During the early years of life, a child’s brain grows fast. Nearly a million neural connections are produced each second. This brain development is also shaped by the experiences that children have during these years. This includes factors such as proper nutrition, social supports, family stability, and much more. It also includes exposure to negative impacts, or adverse childhood experiences.</p>
				<p>The socioeconomic determinants of health are largely responsible for health disparities and inequities. Evidence shows that preventive interventions can have profound, measurable, and long-lasting effects on the health outcomes of children and youth. This includes behavioral health outcomes.</p>
			</div>
			<div class="col-md-6 col-md-pull-6 match-height block-image">
				<img class="img-responsive visible-sm visible-xs" src="<?php echo get_template_directory_uri(); ?>/images/ps-why-is-prevention-science-so-important.png" />
			</div>
		</div>
	</div>
</section>

<section class="component-prevention-science">
	<div class="container">
		<h2 class="text-center">Components of Prevention Science</h2>
		<div class="row">
			<div class="block col-md-15 col-sm-6 match-height">
				<div class="block-inner">
					<h4>Epidemiology</h4>
					<p>The prevalence, distribution, and determinants of the problem in time and space. Epidemiological investigations can be carried out through surveillance and descriptive studies to determine its extent.</p>
				</div>
			</div>
	    <div class="block col-md-15 col-sm-6 match-height">
				<div class="block-inner">
					<h4>Etiology</h4>
					<p>The causes of such positive or negative outcomes, with an emphasis on risk and protective factors.</p>
				</div>
			</div>
	    <div class="block col-md-15 col-sm-6 match-height">
				<div class="block-inner">
					<h4>Efficacy Trials</h4>
					<p>Scientific experiments that test the preventive intervention programs ability to prevent the problem under favorable conditions. Under these optimal conditions, the researcher has control over the intervention and how it is delivered.</p>
				</div>
			</div>
	    <div class="block col-md-15 col-sm-6 match-height">
				<div class="block-inner">
					<h4>Effectiveness Trials</h4>
					<p>Scientific experiments that test the preventive intervention programs ability to prevent the problem under real world conditions. The setting and the kinds of people in the experiment should be very similar to the actual targeted population in a particular location.</p>
				</div>
			</div>
	    <div class="block col-md-15 col-sm-12 match-height">
				<div class="block-inner">
					<h4>Dissemination Research</h4>
					<p>Analyzes how tested and effective prevention intervention programs may be spread to reduce problems at a larger scale.
				</div>
			</div>
		</div>
	</div>
</section>

<section class="adverse-childhood-experiences">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2>Adverse Childhood Experiences (ACES)</h2>
				<p>There is a growing body of research on the profound impact that conditions like abuse, neglect, danger, and loss have on children. Especially those from underserved or at-risk families. Research shows a strong link between ACES and a wide range of physical and mental health problems across the life span.</p>
				<p>Current and emerging research helps us understand the effects of ACEs and toxic stress on the body. This can increase risk for health problems, including chronic disease, mental illness, and obesity. ACES include poor education, abuse/neglect, unemployment and job insecurity, poverty, food insecurity, housing instability, adverse environmental conditions, and limited access to health care.</p>
			</div>
			<div class="info-blocks col-md-6">
				<div class="row">
					<div class="info-block col-sm-6 match-height">
						<div class="info-block-inner">
							<h3>Prevalence</h3>
							<div>
								<h3>59%</h3>
								<p>Percent of U.S. children experience at least one ACE (CDC)</p>
							</div>
							<div>
								<h3>24%</h3>
								<p>Percent of U.S. children experience three or more ACES (CDC)</p>
							</div>
							<div>
								<h3>32%</h3>
								<p>Percent of low income families experience at least one food insecurity – one in six US children (USDA)</p>
							</div>
						</div>
					</div>
					<div class="info-block col-sm-6 match-height">
						<div class="info-block-inner">
							<h3>Impact</h3>
							<div>
								<h3>40+ Diseases</h3>
								<p>Dose-response relationship confirmed between ACES and more than forty medical conditions (CDC)</p>
							</div>
							<div>
								<h3>80%</h3>
								<p>Percent of abused/neglected children will develop a psychiatric disorder (CDC)</p>
							</div>
							<div>
								<h3>80%</h3>
								<p>Increase in risk of overall psychiatric impairment in children from food insecure families*</p>
							</div>
						</div>
					</div>
				</div>
				<p><small>*Murphy, et al 1998, Relationship between hunger and psychosocial functioning in low income American children</small></p>
			</div>
		</div>
	</div>
</section>

<section class="more-information-about-aces">
	<div class="container">
		<h2 class="text-center">More Information About ACES:</h2>
		<div class="row">
			<div class="block col-sm-6 col-md-3">
				<div class="inner-more match-height">
					<h4>CDC-Kaiser Permanente ACE Study</h4>
					<p>One of the largest investigations of 10 types of childhood trauma that affect long-term health. The original study was conducted at Kaiser Permanente from 1995 to 1997 with two waves of data collection.</p>
				</div>
				<a class="btn" href="http://www.ncjfcj.org/sites/default/files/Finding%20Your%20ACE%20Score.pdf" target="_blank">Visit Website</a>
			</div>
			<div class="block col-sm-6 col-md-3">
				<div class="inner-more match-height">
					<h4>Adverse Childhood Experiences Questionnaire (ACE-Q)</h4>
					<p>A tool for families to self-administer the ACE screening tool to determine and interpret ACE scores.</p>
				</div>
				<a class="btn" href="https://www.humankind.org/wp-content/uploads/2016/10/Keynote-Handout-3.pdf" target="_blank">Visit Website</a>
			</div>
			<div class="block col-sm-6 col-md-3">
				<div class="inner-more match-height">
					<h4>Safe Environment for Every Kid (SEEK)</h4>
					<p>The mission of SEEK is to strengthen families, support parents, and thereby promote children's health, development and safety - and help prevent child maltreatment.</p>
				</div>
				<a class="btn" href="http://theinstitute.umaryland.edu/frames/seek.cfm" target="_blank">Visit Website</a>
			</div>
			<div class="block col-sm-6 col-md-3">
				<div class="inner-more match-height">
					<h4>Subsequent ACE studies</h4>
					<p>A list of adapted and expanded ACE surveys to include other types of childhood adversity such as racism, witnessing violence outside of the home, bullying, and involvement with the foster care system.</p>
				</div>
				<a class="btn" href="http://www.acesconnection.com/g/resource-center/blog/resource-list-extended-aces-surveys" target="_blank">Visit Website</a>
			</div>
		</div>
	</div>
</section>

<section class="resilience">
	<div class="container">
		<h2 class="text-center">Resilience</h2>
		<div class="row">
			<p>Resilience refers to the inner resources that can mitigate any harm caused by ACES. Resilience in children has been studied for decades. Yet research on how to measure and promote it has grown only in the past few years.</p>
			<h4>More information about Resilience:</h4>
			<a href="https://www.aap.org/en-us/_layouts/15/WopiFrame.aspx?sourcedoc=/en-us/Documents/RESILIENCE_Questionnaire-1.docx&action=default" target="_blank">American Academy of Pediatrics Resilience Questionnaire</a>
		</div>
	</div>
</section>

<section class="learn-more-prevention-science">
	<div class="container">
		<h2>Where Can I Learn More About Prevention Science?</h2>
		<div class="row">
			<div class="col-md-6">
				<div class="item">
					<div class="link">
						<a href="http://albanypromise.org/" target="_blank">Albany Promise</a>
					</div>
					<p>A cradle-to-career partnership in the City of Albany dedicated to improving the educational outcomes for the children of Albany.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://www.brightfutures.org/tools/" target="_blank">Bright Futures</a>
					</div>
					<p>An American Academy of Pediatrics program dedicated to prevention and health promotion through dissemination of guidelines and resources to primary care providers, families, community organizations, and state partners.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://developingchild.harvard.edu/" target="_blank">Harvard University Center on the Developing Child</a>
					</div>
					<p>A multidisciplinary team committed to driving science-based innovation in policy and practice to achieve breakthrough outcomes for children facing adversity.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="http://www.healthyfamiliesnewyork.org/default.htm" target="_blank">Healthy Families New York</a>
					</div>
					<p>A New York State Office of Children and Family Services program, this is an evidence-based voluntary home visiting model that seeks to improve the health and well-being of infants and children.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="http://www.reachoutandread.org/" target="_blank">Reach Out and Read</a>
					</div>
					<p>A nonprofit organization that gives young children a foundation for success by incorporating books into pediatric care, and encouraging families to read aloud together.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://www.rwjf.org/en/library/research/2014/07/are-the-children-well-.html" target="_blank">Robert Wood Johnson Foundation – “Are the Children Well?”</a>
					</div>
					<p>A report that offers a model and recommendations for promoting the mental wellness of young people, through evidence-based strategies and a focus on wellness.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://link.springer.com/journal/volumesAndIssues/11121" target="_blank">Prevention Science journal</a>
					</div>
					<p>The official journal of prevention science for the Society for Prevention Research.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="item">
					<div class="link">
						<a href="https://melissainstitute.org/" target="_blank">The Melissa Institute for Violence Prevention and Treatment</a>
					</div>
					<p>A non-profit organization dedicated to the study and prevention of violence through education, community service, research support and consultation.</p>
				</div>
				<div class="item featured">
					<div class="link">
						<a href="http://nationalacademies.org/hmd/Reports/2009/Preventing-Mental-Emotional-and-Behavioral-Disorders-Among-Young-People-Progress-and-Possibilities.aspx" target="_blank">The National Academies of Sciences – Health and Medicine Division: “Preventing Mental, Emotional, and Behavioral Disorders Among Young People: Progress and Possibilities”</a>
					</div>
					<p>A report intended to increase interest in prevention practices that can impede the onset or reduce the severity of mental health and substance use disorders among children, youth, and young adults.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="http://www1.nyc.gov/nyc-resources/thrivenyc.page" target="_blank">ThriveNYC</a>
					</div>
					<p>A comprehensive mental health plan for New York City, including resources and Mental Health First Aid trainings.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://www.aap.org/en-us/advocacy-and-policy/aap-health-initiatives/healthy-foster-care-america/Pages/Trauma-Guide.aspx" target="_blank">Trauma Toolbox for Primary Care</a>
					</div>
					<p>A six-part series designed to provide primary care physicians with the tools to address adverse childhood experiences (ACEs) with patients and their families.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="https://youth.gov/youth-topics/youth-mental-health/mental-health-promotion-prevention" target="_blank">Youth.gov – “Promotion and Prevention”</a>
					</div>
					<p>A U.S. government website focused on effective youth programs that defines and considers a variety of promotion and prevention interventions for positive youth mental health outcomes.</p>
				</div>
				<div class="item">
					<div class="link">
						<a href="http://nyztt.org/" target="_blank">Zero to Three Network</a>
					</div>
					<p>Promotes the optimal development of young children, their families, and their communities in the New York metropolitan area.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php require_once('footer.php'); ?>