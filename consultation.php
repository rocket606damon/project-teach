<?php /* Template Name: Consultion */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("consultation_block", "/wp-content/uploads/2017/09/consulation-bg.png",
		"Consultations with <br> Child and Adolescent Psychiatrists",
		"Completely free for all prescribers in New York State who treat children and youth"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
      <div class="consultation_text">
          <div class="container">
              <div class="row">
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
              </div>
          </div>
      </div>
    </div>
<?php require_once('footer.php'); ?>