<?php /* Template Name: Contact Us */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("training-block",
		"/wp-content/uploads/2017/09/contact-bg.png",
		"Contact Project TEACH"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
    </div>
<?php require_once('footer.php'); ?>