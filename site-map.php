<?php /* Template Name: Site Map */ ?>
<?php $titleOverride="Site Map"; require_once('header.php'); ?>
<div class="privacy-plcy">
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
	<div class="sitemaprap" style="padding-top:5px;">
		<div class="vision-mission-block">
			<div class="container">
				<div class="row">
<?php
$header_category = array();
$footer_category = array();
$other_category = array();
$args = array(
    'post_type' =>  'page',
    'post_status'   =>  'publish',
    'numberposts'   =>  -1
);
//Pull pages into an array
$all_pages = get_posts($args);
//loop through the page array
foreach($all_pages as $post) {
    //Set data from the current page as globals,
    //to enable WordPress function tags
    setup_postdata($post);

    //Check whether the page is in the category and
    //send this link to the relevant array
	// header
    if(in_category(3)) {
       $header_category[] = array('order' => $post->menu_order,
        'li' => '			<li class="page_item page-item-' . count($header_category) .
        	'"><a href="'.get_the_permalink().'">'.ucfirst(get_the_title()).'</a></li>');
	// footer
    } else if(in_category(2)) {
		$footer_category[] = array('order' => $post->menu_order,
			'li' =>  '			<li class="page_item page-item-' . count($footer_category) .
        	'"><a href="'.get_the_permalink().'">'.ucfirst(get_the_title()).'</a></li>');
	// other
    } else if(in_category(4)) {
       $other_category[] = array('order' => $post->menu_order,
       		'li' =>  '			<li class="page_item page-item-' . count($other_category) .
        	'"><a href="'.get_the_permalink().'">'.ucfirst(get_the_title()).'</a></li>');
	}
}

usort($header_category, 'build_sorter');
usort($footer_category, 'build_sorter');
usort($other_category, 'build_sorter');

//Restore postdata to before we set it to the current post
wp_reset_postdata();
?>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="our-vision-txt">
					<h3 class="page_list_header">Top-Menu Pages</h3>
					<ul style="margin:10px;list-style: none;">
<?php foreach($header_category as $cat_link) echo $cat_link['li']; ?>
					</ul>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="our-mission-txt">
					<h3 class="page_list_header">Footer Pages</h3>
					<ul style="margin:10px;list-style: none;">
<?php foreach($footer_category as $cat_link) echo $cat_link['li']; ?>
					</ul>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="our-mission-txt">
					<h3 class="page_list_header">Other Pages</h3>
					<ul style="margin:10px;list-style: none;">
<?php foreach($other_category as $cat_link) echo $cat_link['li']; ?>
					</ul>
				</div>
			</div>

				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('footer.php'); ?>
