<?php /* Template Name: Courses Template */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("about_block",
		"2017/09/about-bg.png",
		"CME-Certified Education on Children's Mental Health",
		"Find training that fits your schedule and enhances your ability to care for those you serve",
		1); ?>
<div style="clear: both;"></div>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<section class="up-event-wrap">
    <div class="container-fluid container">
      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 slide-cover">
        <h2 style="font-size: 48px;color: #000;font-weight: 900;letter-spacing: 1px;line-height: 3rem;margin: 0 0 28px 0;">
        	Online Trainings
        </h2>
        <p> Project TEACH offers training in several different formats for pediatric primary care providers (PCPs). These programs support your ability to assess, treat and manage mild-to-moderate mental health concerns in your practice.</p>
        <div class="slider-event">
<?php require(dirname(__FILE__)."/lib/lms_courses/extension.php"); ?>
<?php echo homebaseGetFeatures();?>
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<?php require_once('footer.php'); ?>
