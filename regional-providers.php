<?php /* Template Name: Regional Providers */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("regional-provider",
		"/wp-content/uploads/2017/09/regional-providers-bg.png",
		"Regional Providers",
		"Learn about the teams of child and adolescent psychiatrists and health experts who deliver services for Project&nbsp;TEACH."); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
<?php require_once('footer.php'); ?>