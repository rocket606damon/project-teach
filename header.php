<?php header('Content-Type: text/html; charset=utf-8'); ?>
<?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
        header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<?php $displayTitle = isset($titleOverride) ? $titleOverride : get_the_title(); ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" >
<!--<![endif]-->
<head>
 
<title><?php wp_title(''); ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, minimum-scale=1.0" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta name="format-detection" content="telephone=no">
<meta name="keywords" content="">
<!--script  starts-->
<!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
        <![endif]-->
<!--script  ends-->
<!--common jquery starts-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
<!--common jquery end-->
<!--css styles starts-->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJ7S5MK');</script>
<!-- End Google Tag Manager -->
<script>
  (function(d) {
    var config = {
      kitId: 'hyv7nlq',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<?php wp_head(); ?>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<?php if(is_page(array('test', 'mmh', 'reserve-a-time'))): ?>
<link rel='stylesheet' id='bootstrap-style-css'  href='https://easy-appointments.net/wordpress/wp-content/themes/Bootstrap-3-blank-wordpress-theme-master/css/bootstrap.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='main-style-css'  href='https://easy-appointments.net/wordpress/wp-content/themes/Bootstrap-3-blank-wordpress-theme-master-child/style.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='dwqa-style-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/dw-question-answer/templates/assets/css/style.css?ver=180720161355' type='text/css' media='all' />
<link rel='stylesheet' id='dwqa-rtl-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/dw-question-answer/templates/assets/css/rtl.css?ver=180720161355' type='text/css' media='all' />
<link rel='stylesheet' id='gglcptch-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/google-captcha/css/gglcptch.css?ver=1.37' type='text/css' media='all' />
<link rel='stylesheet' id='wspsc-style-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/wordpress-simple-paypal-shopping-cart/wp_shopping_cart_style.css?ver=4.3.6' type='text/css' media='all' />
<link rel='stylesheet' id='Bootstrap-3-blank-wordpress-theme-master-parent-css'  href='https://easy-appointments.net/wordpress/wp-content/themes/Bootstrap-3-blank-wordpress-theme-master/style.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='ea-admin-awesome-css-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/css/font-awesome.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-style-css'  href='//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='ea-frontend-style-css'  href='https://easy-appointments.net/wordpress/wp-content/plugins/easy-appointments/css/eafront.css?ver=4.9.8' type='text/css' media='all' />
<style id='ea-frontend-style-inline-css' type='text/css'>
body .site-header{padding-top:0}body .entry-content .calendar a{box-shadow:0 0}#booking-overview table{width:100%;border-left:1px dotted #ccc;border-right:1px dotted #ccc;border-bottom:1px dotted #ccc}#booking-overview table td{padding:5px;border-top:1px dotted #ccc}.ea-standard.ea-standard .step input{margin-top:5px;width:63%}.ea-standard.ea-standard .step label{font-weight:400;width:37%}.ea-standard.ea-standard .step select{width:63%;margin-top:5px;display:inline-block} #ui-datepicker-div{display:none}.ea-bootstrap.ea-bootstrap .disabled .block { z-index: 101} .test table tr td:first-child, .mmh table tr td:first-child {padding: 1px;} .test .ea-standard.ea-standard .step input, .mmh .ea-standard.ea-standard .step input {border: 1px solid black;}
</style>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css<?php echo '?' . filemtime(get_stylesheet_directory() . '/style.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css?<?php echo filemtime(get_template_directory_uri() . '/css/responsive.css'); ?>" media="screen" >
<!--css styles ends-->
<script>
  var $ = jQuery.noConflict();
  $(document).ready(function(){
    var url = document.location.toString();
    if(url.match('#')){
      $('.nav-pills a[data-target="#' + url.split('#')[1] + '"]').tab('show');
    }

    $('.nav-pills a').on('shown.bs.tab', function(e){
      window.location.hash = e.target.hash;
    });

    $('#tabs').tabs({
      activate: function(event, ui){
        var active = $('#tabs').tabs('option', 'active');
      }
    });
  });
</script>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJ7S5MK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">
  <!--header section starts-->
  <header>
    <div class="header-main cf">
      <div class="container">
       <div class="row">
        <!-- header section coding starts here-->
        <div class="col-md-4 col-sm-3 col-xs-12 header-logo">
            <a href="<?php echo get_link_by_slug("home"); ?>" title="" >
              <img src="<?php echo get_pt_attachment_url('2017/09/header-logo.png'); ?>" alt="Project TEACH" title="Project TEACH" />
            </a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
        </div>
        <div class="col-md-8 col-sm-9 col-xs-12">
           <div class="search_box">
              <form action="/">
              <ul class="search_box">
                   <li><input id="searchtextBox" class="search-field" placeholder="Search" value="" name="s" type="search" tabindex="1">
                       <button class="search_submit" type="submit" title="Search" tabindex="2"></button>
                   </li>
                   <li><a href="#" title="Translate Page" class="btn-translate">Translate Page</a></li>
               </ul>
               </form>
               <div id="google_translate_element"></div>
           </div>
        </div>
        <div class="cf"></div>
        <nav class="navbar navbar-right">
          <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbar-collapse">

              <?php
              wp_nav_menu( array(
                  'theme_location'    => 'primary',
                  'depth'             => 2,
                  'container'         => 'div',
                  'container_class'   => 'collapse navbar-collapse',
                  'container_id'      => 'primary-navbar-collapse',
                  'menu_class'        => 'nav navbar-nav',
                  'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                  'walker'            => new WP_Bootstrap_Navwalker())
              );
              ?>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <!-- header section coding ends here-->
        </div>
      </div>
    </div>

    <?php if(is_page('intensive-training')): ?>
    <div class="submenu">
      <div class="container">
        <ul>
          <li><a href="#faculty" title="Faculty">Faculty</a></li>
          <li><a href="#agenda" title="Agenda">Agenda</a></li>
          <li><a href="#cme-info" title="CME Info">CME Info</a></li>
        </ul>
      </div>
    </div>
    <?php endif; ?>

    <?php if(is_front_page() | is_page(array('consultation', 'referrals'))): ?>
    <div class="modal" id="welcome-modal" tabindex="-1" role="dialog" aria-labelledby="welcome-modal-Label">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="welcome-modal-label">Get Involved with Project TEACH</h4>
          </div>
          <div class="modal-body">
            <div class="embed-responsive embed-responsive-16by9">
              <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ejx9zpFrsd8"></iframe> -->
              <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/JKkt-L8xSa8?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <p class="standard">Learn how Project TEACH helps pediatric primary care providers.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning nothanks" data-dismiss="modal">Please Don't Show This Again</button>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>

<?php get_header(); ?>

  </header>
  <!--header section ends-->
