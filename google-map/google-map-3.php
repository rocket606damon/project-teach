<script>
  function initMap() {
    var uluru = {lat: 40.758024, lng: -73.9771662};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: uluru,
      mapTypeId: 'roadmap'
    });
    //CREATE A CUSTOM PIN ICON
    var image ='<?php bloginfo('template_directory'); ?>/ed_images/marker.png';
    var markerNew = new google.maps.Marker({
    	position: uluru,
    	map: map,
    	icon: image
    });
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2L-vq0dV2Za6m_FxxIxL8Npiy185INes&callback=initMap"></script>