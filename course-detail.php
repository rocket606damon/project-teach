<?php /* Template Name: Course Detail Template */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("about_block",
		"2017/09/about-bg.png",
		"CME-Certified Education on Children's Mental Health",
		"Find training that fits your schedule and enhances your ability to care for those you serve",
		"1"); ?>
<div style="clear: both;"></div>
    <div class="home-comman-text">
        <div class="container">
            <div class="row">
                 <ol class="breadcrumb">
                    <li><a href="<?php echo get_link_by_slug("home"); ?>" title="HOME">HOME</a>
                    <a href="<?php echo get_link_by_slug("training"); ?>" title="TRAINING">TRAINING</a> >
                    <a href="<?php echo get_link_by_slug("course-catalog"); ?>" title="COURSE CATALOG">COURSE CATALOG</a> >
						<p>COURSE DETAIL</p>
					</li>
                  </ol>
            </div>
        </div>
     </div>
<section class="up-event-wrap">
    <div class="container-fluid">
      <div class="col-md-12 col-sm-12 col-xs-12 slide-cover">
<?php require(dirname(__FILE__)."/lib/lms_courses/extension.php"); ?>
<?php $number = isset($_REQUEST['number']) ? $_REQUEST['number'] : 1; ?>
<?php echo homebaseGetFeatureCourseDetails($number);?>
      </div>
    </div>
  </section>
</div>
<?php require_once('footer.php'); ?>
