<?php /* Template Name: Referrals */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("referrals-block",
		"/wp-content/uploads/2017/09/referrals-bg.png",
		"Find the Services that Children and Families Need",
		"Get help with linkage and referrals to intervention, treatment, and support services for children and families"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php 	the_content(); ?>
<?php endwhile; ?>
<?php require_once('footer.php'); ?>