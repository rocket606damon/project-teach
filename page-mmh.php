<?php
/*
Template Name: mmh
*/
require_once('header.php'); ?>


	<style>
		body.mmh .expanded-services--title {
			background-color: #e09b3d;
			width: 100%;
			padding: 3rem;
			text-align: center;
			clip-path: polygon(0 0, 0% 75%, 50% 100%, 100% 75%, 100% 0);
		}

		body.mmh .expanded-services--title h2 {
			color: white;
			margin-bottom: 0;
		}
		body.mmh .banner-text{
			padding-bottom:0!important;
		}
		.expanded-services--subtitle,
		.linkage-block--subtitle {
			font-weight: 600;
			color:white;
			padding:12px;
		}
		.expanded-services--subtitle{
			background-color:#039fda;
			margin-top:24px;
		}
		.linkage-block--subtitle{
			background-color:#3a0e79;
		}
		.learn-more--subtitle{
			color:#3a0e79;
			padding:0;
		}
		.video-gallery--subtitle {
			font-weight: 600;
			color: white;
			text-align: center;
		}

		.expanded-services--content {
			padding-top: 2rem;
		}

		.expanded-services--body-text {
			max-width: 600px;
			min-width: 300px;
		}

		body.mmh .expanded-services-block .standard,
		body.mmh .learn-more-prevention-science .standard {
			font-size: 18px;
			font-weight: 500;
			line-height: 1.4rem;
		}

		.linkage-block {
			background: #fff;
			padding: 97px 0 91px 0;
		}

		.video-gallery--block {
			background-color: #039fda;
			background-image:url(http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/ps-pattern-bg-30.png);
			padding: 97px 0 91px 0;
		}

		.video-gallery--video {
			border-top: 20px solid transparent;

		}

		body.mmh iframe {
			border-radius: 6px;
			width: 100%;
		}

		body.mmh .learn-more-prevention-science .panel-orange .panel-title a:before,
		body.mmh .learn-more-prevention-science .panel-orange .panel-title {
			color: #e19c3c;
		}

		body.mmh .expanded-services--content h3 {
			text-align: center;
			line-height: normal!important;
		}

		body.mmh .expanded-services--content .btn-cover {
			margin-bottom: 2rem;
		}

		body.mmh .btn-cover .btn {
			background: #e19c3c;
			border-color: #e19c3c;
		}

		body.mmh .btn-cover .btn:hover {
			background: white;
			color: #e19c3c;
		}

		body.mmh .consultation_dif .btn-cover .btn {
			background: #3a0e79;
			border-color: #3a0e79;
		}

		body.mmh .consultation_dif .btn-cover .btn:hover {
			background: white;
			color: #3a0e79;
		}

		body.mmh .pdf .pdf-resource {
			display: flex;
			width:100%;
			background-color:#7bbf43;
		}
		body.mmh .pdf-resource .inner{
			background-color:#699e30;
		}
		body.mmh .btn-cover{
			text-align: center;
			padding:0 0 4rem;
		}
		body.mmh .btn-cover:last-child {
			padding:2rem 0 4rem;
		}
		body.mmh .pdf-resource span.inside {
			margin: 0;
			margin-left: 2rem;
			padding-right: 1rem;
			float: none;
			align-self: center;
			width: 100%;
		}

		body.mmh .pdf-resource .inner {
			float: none;
			margin-left: auto;
		}

		body.mmh .pdf .view {
			text-align: center;
			margin-bottom: 2rem;
		}

		body.mmh .pdf-resource button {
			border: none;
			border-radius: 0 16px 16px 0;
		}

		body.mmh .modal {
			top: 20%;
		}

		body.mmh .fa {
			color: #fff;
			font-size: 30px;
		}
		body.mmh .fa-envelope,
		body.mmh .fa-calendar,
		body.mmh .fa-clock-o	{
			font-size: 60px;
		}
		body.mmh .consultation__box .inner{
			transition:all .5s ease-in-out 0s;
		}
		body.mmh .inner__icon-purple{
			background-color:#3a0e79;
		}
		body.mmh .consultation__box .inner:hover{
			background-color:white;
		}
		body.mmh .consultation__box .inner__icon-blue{
			border:2px solid #039fda;
		}
		body.mmh .consultation__box .inner__icon-green{
			border:2px solid #7bbf43;
		}
		body.mmh .consultation__box .inner__icon-purple{
			border:2px solid #3a0e79;
		}
		.inner__icon-blue:hover i{
			color:#039fda;
		}
		.inner__icon-purple:hover i{
			color:#3a0e79;
		}
		.inner__icon-green:hover i{
			color:#7bbf43;
		}
		.month {
			padding: 25px;
			width: 100%;
			background: #039fda;
			text-align: center;
		}

		.month ul {
			margin: 0;
			padding: 0;
			list-style: none;
		}

		.month ul li {
			color: white;
			font-size: 20px;
			text-transform: uppercase;
			letter-spacing: 3px;
		}

		.month .prev {
			float: left;
			padding-top: 10px;
		}

		.month .next {
			float: right;
			padding-top: 10px;
		}

		.weekdays {
			margin: 0;
			padding: 10px 0;
			background-color: #ddd;
		}

		.weekdays li {
			display: inline-block;
			width: 13.4%;
			color: #666;
			text-align: center;
		}

		.days {
			padding: 10px 0;
			background: #eee;
			margin: 0;
		}

		.days li {
			list-style-type: none;
			display: inline-block;
			width: 13.4%;
			text-align: center;
			margin-bottom: 5px;
			font-size: 12px;
			color: #777;
		}

		.days li .active {
			padding: 5px;
			width: 25px;
			display: inline-block;
			background: #039fda;
			color: white !important;
			transition: all .3s;
		}

		.days li .active:hover {
			background: #7bbf43;
		}
		body.mmh .disabled{
			opacity:.5;
			pointer-events: none;
		}
		body.mmh .today{
			color:#7bbf43;
			font-weight: 600;
		}
		body.mmh .days .today a span{
			color:#7bbf43 !important;
		}
		body.mmh li.next, body.mmh li.prev{
			width:40px; height:40px;
			transition: all .3s;
			border-radius:50%;
		}
		body.mmh li.next:hover, body.mmh li.prev:hover{
			background-color:#054f6d;
		}
		.learn-more__title--women{color:#e19c3c;}
		.learn-more__title--providers{color:#7bbf43;}
		body.mmh input[type=text],
		select,
		textarea,
		input[type=email] {
			width: 100%;
			padding: 12px;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
			margin-top: 6px;
			margin-bottom: 16px;
			resize: vertical;
		}

		body.mmh body.mmh .consultation__box {
			max-width: 400px;
			min-width:31%;
			display: inline-block;
			text-align: center;
		}
		body.mmh .component-positive-impacts{
			background-image:none;
		}
		body.mmh .video-intro {
			background-color:#eaeaea;
		}
		body.mmh .component-positive-impacts img{
			margin:auto;
		}
		body.mmh .component-positive-impacts .component__subtitle{
			color:#3a0e79;
			font-size:18px;
			margin-bottom:4rem;
		}
		body.mmh .component-positive-impacts .component__row{
			align-items:center;
		}
		body.mmh .linkage-block .row {
			display:flex;
		}
		body.mmh .video-gallery--block .row{
			display:flex;
			flex-wrap:wrap;
		}
		body.mmh .featured-faculty .row{
			display:flex;
			flex-wrap:wrap;
		}
		body.mmh .featured-faculty .box-height {
			height:auto!important;
		}
		body.mmh .featured-faculty .faculty-info {
			display:flex; align-items:center; justify-content:center;
		}
		body.mmh .expanded-services-block .flex-row{
			display:flex;
			flex-wrap:wrap;
		}
		body.mmh .expanded-services-block .border-column{
			border-right:1px solid #efefef;
			border-bottom:1px solid #efefef;
		}
		body.mmh .expanded-services-block .border-column-bottom{
			border-bottom:1px solid #efefef;
		}
		body.mmh .expanded-services-block .expanded__text--container ul{

		}
		body.mmh .expanded-services-block .expanded__text--container ul{
			list-style:none; font-size:18px; text-align:center; padding-bottom:25px;
		}
		body.mmh .consultation__box .inner {
			min-width: 50px;
			width: 100%;
			height: 125px;
			border-radius: 16px;
			/* border: none; */
			display: flex;
			align-items:center;
			justify-content: center;
		}
		body.mmh .consultation__box:first-child button, body.mmh .consultation__box:first-child a {
			background-color: #039fda;
		}

		body.mmh .consultation__box:last-child button, body.mmh .consultation__box:last-child a {
			background-color: #7bbf43;
		}
		body.mmh footer{
			margin-top:0;
		}
		body.mmh .bio-info-innner{
			padding:0 15px;
		}
		body.mmh input[type=submit] {
			background-color: #7bbf43;
			color: white;
			padding: 12px 20px;
			border: none;
			border-radius: 4px;
			cursor: pointer;
		}
		body.mmh .consultation_map img{
			max-width:100%;
		}
		body.mmh .video-intro h2{
			color:#039fda;
		}
		body.mmh .video-intro .video-description p:last-child{
			margin-top:auto;
		}
		body.mmh .video-intro .video-flex{
			display:flex;
			align-items:center;
			flex-wrap:wrap
		}
		body.mmh .video-intro .video-panel img{
			max-width:100%;
		}
		body.mmh .video-title {
			padding-bottom: 8px;
		}
		body.mmh .video-intro .video-description .video-title{
			font-size:42px;
		}
		body.mmh .consultation__box .link-wrapper .inside{
			margin-top:8px;
		}
		body.mmh .our-team ul li {
			width:auto;
			max-width:100%;
		}
		body.mmh input[type=submit]:hover {
			background-color: #69a339;
		}

		body.mmh .video-gallery--block p {
			color: white;
			font-size: 18px;
		}
		body.mmh iframe{
			height:auto;
			min-height:225px;
		}
		body.mmh .home-comman-text .row{
			display:flex;
		}
		.video-gallery--subtitle {
			font-size: 2rem;
		}

		body.mmh .banner {
			background-color: #3a0e79;
			background-position: center 45%;
		}
		body.mmh .bio-info-innner {
			bottom:0;
			top:8%;
		}
		body.mmh .linkage-block {
			padding-bottom: 50px;
		}
		body.mmh #faculty .info-bio p{
			line-height: 1.5;
		}
		body.mmh #gform_fields_1{
			list-style: none;
		}
		/* Add media queries for smaller screens */
		@media screen and (min-width:1550px) {
			body.mmh iframe{
				height:auto;
				min-height:300px;
			}
		}
		@media (max-width: 768px) {
			body.mmh .linkage-block .row{
				flex-wrap:wrap;
			}
			body.mmh .video-gallery--block .video-title {
				max-width:400px;
				margin:0 auto;
			}
			body.mmh .featured-faculty .our-team{
				margin:0 auto;
			}
		}
		@media screen and (max-width:720px) {
			.weekdays li,
			.days li {
				width: 13.1%;
			}
		}

		@media screen and (max-width:576px) {
			body.mmh .component-positive-impacts{
				padding:60px 15px;
			}
			body.mmh .service-txt{
				padding:20px 0;
			}
			body.mmh .video-intro .video-description .video-title{
				font-size:28px;
			}
		}

		@media screen and (max-width: 420px) {
			.weekdays li,
			.days li {
				width: 12.5%;
			}
			.days li .active {
				padding: 2px;
			}
		}

		@media screen and (max-width: 290px) {
			.weekdays li,
			.days li {
				width: 12.2%;
			}
		}
	</style>


	<div class="banner" style="background-image: url(http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/iStock-872983526-gradient2.png)">
		<div class="container">
			<div class="row">
				<div class="banner-text col-md-8 col-sm-12 col-xs-12" style="padding-top:5%;padding-bottom:5%;">
					<h1 class="banner-title">Maternal Mental Health Initiative</h1>
					<p class="banner-caption">In 2018, New York State launched a broad effort to combat maternal depression. Maternal depression and related mood and anxiety disorders are prevalent. If you can identify and treat these conditions early, it leads to better health outcomes for
						mothers and children.</p>
						<p><strong>Project TEACH is a part of this cross-systems effort.</strong></p>
				</div>
			</div>
		</div>
	</div>


	<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>


	<!-- <div class="video-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 video-flex">
					<div class="col-md-5 col-sm-12 video-description">
						<h2>Maternal Mental Health Initiative</h2>
						<p class="standard">In 2018, New York State launched a broad effort to combat maternal depression. Maternal depression and related mood and anxiety disorders are prevalent. If you can identify and treat these conditions early, it leads to better health outcomes for
							mothers and children.</p>
						<p><strong>Project TEACH is a part of this cross-systems effort.</strong></p>
					</div>
					<div class="col-md-7 col-sm-12 video-panel">
						<img src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/iStock-872983526.jpg">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe src="https://player.vimeo.com/video/293627056" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<section class="component-positive-impacts">
		<div class="container">
			<div class="row component__row">
				<h2><strong>Project TEACH helps you provide the best possible care to women both during and after pregnancy</strong></h2>
				<p class="component__subtitle"><strong>This initiative gives you direct access to mental health experts who can provide guidance on how to:</strong></p>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="service-txt">
						<!-- <div class="one number">1</div> -->
						<img class="img-responsive" src="https://projectteachny.org/wp-content/uploads/2018/10/orange-icon-400-01.png" />
						<p>Identify effective evidence-based screening and treatment strategies</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="service-txt">
						<!-- <div class="two number">2</div> -->
						<img class="img-responsive" src="https://projectteachny.org/wp-content/uploads/2018/10/blue-icon-400-01.png" />
						<p>Help support mothers and their families</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="service-txt">
						<!-- <div class="tree number">3</div> -->
						<img class="img-responsive" src="https://projectteachny.org/wp-content/uploads/2018/10/green-icon-400-01.png" />
						<p>Find linkages and referrals to community-based resources</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="video-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 video-flex">
					<div class="col-md-5 col-sm-12 video-description">
						<h4 class="video-title">1 in 7 moms and 1 in 10 dads experience postpartum depression*</h4>
					</div>
					<div class="col-md-7 col-sm-12 video-panel">
						<img style="display:block; margin:0 auto; margin-bottom:12px;" src="http://project-teach.launchpaddev.com/wp-content/themes/project-teach/images/affection-baby-beautiful.jpg">
						<a style="text-align:right; display:block; margin:0 auto;" class="standard" href="http://www.postpartum.net/">* www.postpartum.net</a>
						<!-- <div class="embed-responsive embed-responsive-16by9">
							<iframe src="https://player.vimeo.com/video/293627056" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="expanded-services-block">
		<div class="container-fluid">
			<div class="row component__row">
				<div class="expanded-services--title">
					<h2>Services available through the Maternal Mental Health Initiative include:</h2>
				</div>
			</div>
			<div class="row flex-row">
				<div class="col-sm-12 col-md-6 border-column">
					<div class="expanded-services--content">
						<h2 class="expanded-services--subtitle">Consultation Services</h2>
						<div class="expanded__text--container">
							<p class="standard">New York maternal health providers can join and benefit from case-based learning with their peers. Consultation with expert psychiatrists in maternal mental health is available through an open conference line. Interactive consultation calls take place 2 times per week.*</p>
							<ul>
								<li><strong>Tuesdays</strong> 3pm - 4pm</li>
								<li><strong>Thursdays</strong> 1pm - 2pm</li>
							</ul>
							<p class="standard">The hour-long open conference calls will start with facilitated discussion followed by the opportunity to ask specific questions. Any provider may schedule in advance a specific ten-minute window to call in and present a brief case or question and get a response. Providers may join the entire call or call in only for their scheduled consultation time.</p>
							<p class="standard">Providers can also submit non-urgent questions by email. An expert physician will respond within 2 business days.</p>
						</div>
					</div>
					<div class="row" style="display:flex; justify-content:center; margin:2rem;">
						<div class="pdf col-md-12 col-sm-10">
							<div class="row">
							<div class="consultation__box col-sm-4" style="padding-left:8px; padding-right:8px;">
								<div class="link-wrapper">
									<button class="inner inner__icon-blue" data-toggle="modal" data-target="#calendar-modal"><i class="fa fa-calendar" aria-hidden="true"></i></button>
									<p class="inside">Participate in an upcoming call: view calendar</p>
								</div>
							</div>
							<div id="calendar-modal" class="modal fade" role="dialog">
								<div class="modal-dialog">

									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title" style="color:#039fda;">Calendar of Upcoming Calls</h4>
										</div>
										<div class="modal-body">
											<div id="october">
												<div class="month">
													<ul>
														<li id="october-prev" class="prev" style="opacity:.5; background-color:transparent;">&#10094;</li>
														<li id="october-next" class="next">&#10095;</li>
														<li>October<br>
															<span>2018</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days" id="october-days">
													<li>1</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">2</span></a></li>
													<li>3</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">4</span></a></li>
													<li>5</li>
													<li>6</li>
													<li>7</li>
													<li>8</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">9</span></a></li>
													<li>10</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">11</span></a></li>
													<li>12</li>
													<li>13</li>
													<li>14</li>
													<li>15</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">16</span></a></li>
													<li>17</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">18</span></a></li>
													<li>19</li>
													<li>20</li>
													<li>21</li>
													<li>22</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">23</span></a></li>
													<li>24</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">25</span></a></li>
													<li>26</li>
													<li>27</li>
													<li>28</li>
													<li>29</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">30</span></a></li>
													<li>31</li>
												</ul>
											</div>
											<div id="november" style="display:none;">
												<div class="month">
													<ul>
														<li id="november-prev" class="prev">&#10094;</li>
														<li id="november-next" class="next">&#10095;</li>
														<li>November<br>
															<span>2018</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li></li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">1</span></a></li>
													<li>2</li>
													<li>3</li>
													<li>4</li>
													<li>5</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">6</span></a></li>
													<li>7</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">8</span></a></li>
													<li>9</li>
													<li>10</li>
													<li>11</li>
													<li>12</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">13</span></a></li>
													<li>14</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">15</span></a></li>
													<li>16</li>
													<li>17</li>
													<li>18</li>
													<li>19</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">20</span></a></li>
													<li>21</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">22</span></a></li>
													<li>23</li>
													<li>24</li>
													<li>25</li>
													<li>26</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">27</span></a></li>
													<li>28</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">29</span></a></li>
													<li>30</li>
												</ul>
											</div>
											<div id="december" style="display:none;">
												<div class="month">
													<ul>
														<li id="december-prev" class="prev">&#10094;</li>
														<li id="december-next" class="next">&#10095;</li>
														<li>December<br>
															<span>2018</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li>1</li>
													<li>2</li>
													<li>3</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">4</span></a></li>
													<li>5</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">6</span></a></li>
													<li>7</li>
													<li>8</li>
													<li>9</li>
													<li>10</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">11</span></a></li>
													<li>12</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">13</span></a></li>
													<li>14</li>
													<li>15</li>
													<li>16</li>
													<li>17</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">18</span></a></li>
													<li>19</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">20</span></a></li>
													<li>21</li>
													<li>22</li>
													<li>23</li>
													<li>24</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">25</span></a></li>
													<li>26</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">27</span></a></li>
													<li>28</li>
													<li>29</li>
													<li>30</li>
													<li>31</li>
												</ul>
											</div>
											<div id="january" style="display:none;">
												<div class="month">
													<ul>
														<li id="january-prev" class="prev">&#10094;</li>
														<li id="january-next" class="next">&#10095;</li>
														<li>January<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">1</span></a></li>
													<li>2</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">3</span></a></li>
													<li>4</li>
													<li>5</li>
													<li>6</li>
													<li>7</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">8</span></a></li>
													<li>9</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">10</span></a></li>
													<li>11</li>
													<li>12</li>
													<li>13</li>
													<li>14</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">15</span></a></li>
													<li>16</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">17</span></a></li>
													<li>18</li>
													<li>19</li>
													<li>20</li>
													<li>21</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">22</span></a></li>
													<li>23</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">24</span></a></li>
													<li>25</li>
													<li>26</li>
													<li>27</li>
													<li>28</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">29</span></a></li>
													<li>30</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">31</span></a></li>
												</ul>
											</div>
											<div id="february" style="display:none;">
												<div class="month">
													<ul>
														<li id="february-prev" class="prev">&#10094;</li>
														<li id="february-next" class="next">&#10095;</li>
														<li>February<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li>1</li>
													<li>2</li>
													<li>3</li>
													<li>4</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">5</span></a></li>
													<li>6</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">7</span></a></li>
													<li>8</li>
													<li>9</li>
													<li>10</li>
													<li>11</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">12</span></a></li>
													<li>13</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">14</span></a></li>
													<li>15</li>
													<li>16</li>
													<li>17</li>
													<li>18</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">19</span></a></li>
													<li>20</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">21</span></a></li>
													<li>22</li>
													<li>23</li>
													<li>24</li>
													<li>25</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">26</span></a></li>
													<li>27</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">28</span></a></li>
												</ul>
											</div>
											<div id="march" style="display:none;">
												<div class="month">
													<ul>
														<li id="march-prev" class="prev">&#10094;</li>
														<li id="march-next" class="next">&#10095;</li>
														<li>March<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li>1</li>
													<li>2</li>
													<li>3</li>
													<li>4</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">5</span></a></li>
													<li>6</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">7</span></a></li>
													<li>8</li>
													<li>9</li>
													<li>10</li>
													<li>11</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">12</span></a></li>
													<li>13</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">14</span></a></li>
													<li>15</li>
													<li>16</li>
													<li>17</li>
													<li>18</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">19</span></a></li>
													<li>20</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">21</span></a></li>
													<li>22</li>
													<li>23</li>
													<li>24</li>
													<li>25</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">26</span></a></li>
													<li>27</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">28</span></a></li>
													<li>29</li>
													<li>30</li>
													<li>31</li>
												</ul>
											</div>
											<div id="april" style="display:none;">
												<div class="month">
													<ul>
														<li id="april-prev" class="prev">&#10094;</li>
														<li id="april-next" class="next">&#10095;</li>
														<li>April<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li>1</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">2</span></a></li>
													<li>3</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">4</span></a></li>
													<li>5</li>
													<li>6</li>
													<li>7</li>
													<li>8</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">9</span></a></li>
													<li>10</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">11</span></a></li>
													<li>12</li>
													<li>13</li>
													<li>14</li>
													<li>15</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">16</span></a></li>
													<li>17</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">18</span></a></li>
													<li>19</li>
													<li>20</li>
													<li>21</li>
													<li>22</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">23</span></a></li>
													<li>24</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">25</span></a></li>
													<li>26</li>
													<li>27</li>
													<li>28</li>
													<li>29</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">30</span></a></li>
												</ul>
											</div>
											<div id="may" style="display:none;">
												<div class="month">
													<ul>
														<li id="may-prev" class="prev">&#10094;</li>
														<li id="may-next" class="next">&#10095;</li>
														<li>May<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li>1</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">2</span></a></li>
													<li>3</li>
													<li>4</li>
													<li>5</li>
													<li>6</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">7</span></a></li>
													<li>8</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">9</span></a></li>
													<li>10</li>
													<li>11</li>
													<li>12</li>
													<li>13</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">14</span></a></li>
													<li>15</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">16</span></a></li>
													<li>17</li>
													<li>18</li>
													<li>19</li>
													<li>20</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">21</span></a></li>
													<li>22</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">23</span></a></li>
													<li>24</li>
													<li>25</li>
													<li>26</li>
													<li>27</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">28</span></a></li>
													<li>29</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">30</span></a></li>
													<li>31</li>
												</ul>
											</div>
											<div id="june" style="display:none;">
												<div class="month">
													<ul>
														<li id="june-prev" class="prev">&#10094;</li>
														<li id="june-next" class="next" style="opacity:.5 background-color:transparent;">&#10095;</li>
														<li>June<br>
															<span>2019</span>
														</li>
													</ul>
												</div>

												<ul class="weekdays">
													<li>Mo</li>
													<li>Tu</li>
													<li>We</li>
													<li>Th</li>
													<li>Fr</li>
													<li>Sa</li>
													<li>Su</li>
												</ul>

												<ul class="days">
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li></li>
													<li>1</li>
													<li>2</li>
													<li>3</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">4</span></a></li>
													<li>5</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">6</span></a></li>
													<li>7</li>
													<li>8</li>
													<li>9</li>
													<li>10</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">11</span></a></li>
													<li>12</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">13</span></a></li>
													<li>14</li>
													<li>15</li>
													<li>16</li>
													<li>17</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">18</span></a></li>
													<li>19</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">20</span></a></li>
													<li>21</li>
													<li>22</li>
													<li>23</li>
													<li>24</li>
													<li><a href="https://zoom.us/webinar/register/WN_UMYAWVJcQX6YcdpSL2E16Q" target="_blank"><span class="active">25</span></a></li>
													<li>26</li>
													<li><a href="https://zoom.us/webinar/register/WN_bonaTeAeT0-UZD2HRRFctg" target="_blank"><span class="active">27</span></a></li>
													<li>28</li>
													<li>29</li>
													<li>30</li>
												</ul>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
							<div class="consultation__box col-sm-4" style="padding-left:8px; padding-right:8px;">
								<div class="link-wrapper">
									<a class="inner inner__icon-purple" href="https://projectteachny.org/reserve-a-time/"><i class="fa fa-clock-o" aria-hidden="true"></i></a>
								</div>
								<p class="inside" style="margin-top:8px;">Reserve a time for your question</p>
							</div>
							<div id="warm-line-modal" class="modal fade" role="dialog">
								<div class="modal-dialog">

									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title" style="color:#7bbf43;">Ask a Question</h4>
										</div>
										<div class="modal-body">
											<?php echo do_shortcode("[gravityform id='1']"); ?>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
							<div class="consultation__box col-sm-4" style="padding-left:8px; padding-right:8px;">
								<div class="link-wrapper">
									<a class="inner inner__icon-green" href="https://ez372.infusionsoft.app/app/form/non-urgent-mmh-question-submission-form?cookieUUID=bd587dd6-4403-4344-a8bc-12c669822bc0"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									<p class="inside">Email a non-urgent question</p>
								</div>
							</div>
							</div>
						</div>
					</div>
					<p class="expanded-services--body-text" style="text-align:center; margin:auto;">*Dates and times subject to change, please check the calendar for exact dates and times.</p>
				</div>
				<div class="col-sm-12 col-md-6 border-column-bottom">
					<div class="expanded-services--content">
						<h2 style="background-color:#7bbf43;" class="expanded-services--subtitle">Education</h2>
						<div class="expanded__text--container">
							<p class="standard">Get information on educational programs being offered.</p>
						</div>
						<div class="row" style="display:flex; justify-content:center; flex-wrap:wrap;">
							<div class="pdf col-xs- 12 col-sm-8 col-md-8">
								<div class="pdf-resource cf">
									<span class="inside">Diagnosis and Treatment of Depression During Pregnancy</span>
									<a class="inner" href="https://projectteachny.org/course-detail/?number=6" target="_blank"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</div>
								<div class="pdf-resource cf">
									<span class="inside">Screening and Treatment of Postpartum Depression</span>
									<a class="inner" href="https://projectteachny.org/course-detail/?number=7" target="_blank"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</div>
								<a class="view" href="https://projectteachny.org/live-training/online-courses/" target="_blank">View All Trainings</a>
							</div>
							<div class="col-sm-12 col-md-8">
								<img style="max-width:100%; margin:2rem auto; display:block;" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/iStock-901666754.jpg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="linkage-block">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12">
					<div class="consultation_map">
						<figure><img src="https://projectteachny.org/wp-content/uploads/2018/10/Project-TEACH-New-York-State-FullMap_Counties_Updated_9-12-1.jpg" alt="involved Map"></figure>
					</div>
					<div class="btn-cover">
						<a href="https://projectteachny.org/wp-content/uploads/2018/10/Project-TEACH-New-York-State-FullMap_Counties_Updated_9-12-1.jpg" title="View Event Details" class="btn">View Counties</a>
					</div>
				</div>
				<div class="col-md-7 col-sm-12 col-xs-12">
					<div class="consultation_dif">
						<h2 class="linkage-block--subtitle">Linkages & Referrals</h2>
						<p class="standard">Our Regional Providers are here to help you find linkages and referrals. Resources are available by calling Project TEACH Regional Provider in your area. Locate your county on the map. Then use the contact information for your region to request
							linkage and referral information.</p>
						<div class="btn-cover">
							<a href="http://projectteachny.org/regional-providers/" title="View Event Details" class="btn">View Regional Providers</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="learn-more-prevention-science">
		<div class="container">
			<h2 class="learn-more--subtitle">Resources</h2>
			<!-- <p class="standard">Questions? Contact the Project TEACH Statewide Coordination Center at (877) 709-1771, or info@projectteachny.org</p> -->
			<div class="row">
				<div class="col-md-12">
					<h3 class="learn-more__title learn-more__title--providers">Providers</h3>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseFive" aria-expanded="false">Maternal Mental Health Resources</a>
								</h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org" target="_blank">womensmentalhealth.org</a>
										</div>
										<p>Resources from MGH Center for Women’s Mental Health</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.step-ppd.com/" target="_blank">Support and Training to Enhance Primary Care (STEP)-PPD</a>
										</div>
										<p>Provides web-based education to give primary care providers up-to-date information on evidence-based approaches for assessing and treating PPD. The website includes case studies, interactive video clips, didactic information and links to additional
											resources.
										</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.acog.org/" target="_blank">The American College of Obstetricians and Gynecologists</a>
										</div>
										<p>ACOG provides a FAQs sheet on perinatal depression and offers a list of resources for patients and clinicians.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.aafp.org/patient-care/nrn/studies/all/trippd/ppd-toolkit.html" target="_blank">American Academy of Family Physicians/ The Postpartum Depression Toolkit</a>
										</div>
										<p>includes educational slides that explain screening and follow-up of postpartum depression; tools for screening, diagnosis, and selecting therapy; tools to facilitate nurse follow-up calls and patient self-care; and recommendations for monitoring
											the progression of depressive symptoms.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/specialty-clinics/psychiatric-disorders-during-pregnancy/" target="_blank">Psychiatric Disorders During Pregnancy</a>
										</div>
										<p>A USAID project, Youth Power seeks to improve the capacity of youth-led and youth-serving institutions through evidence-based positive youth development.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/specialty-clinics/postpartum-psychiatric-disorders/" target="_blank">Postpartum Psychiatric Disorders</a>
										</div>
										<p>Specialty Areas > Postpartum Psychiatric Disorders</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://safehealthcareforeverywoman.org/safety-action-series/presentation-of-maternal-mental-health-patient-safety-bundle-perinatal-depression-and-anxiety/" target="_blank">Council on Patient Safety in Women’s Health Care: Maternal Mental Health Safety Bundle</a>
										</div>
										<p>The National Council on Patient Safety in Women’s Health Care created the Maternal Mental Health Safety Bundle, and a list of resources for patients and providers.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
					        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">Resources for Addressing Medication Treatment</a>
					      </h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="https://toxnet.nlm.nih.gov/newtoxnet/dart.htm" target="_blank">DART- Developmental and Reproductive Toxicology Database</a>
										</div>
										<p>DART provides more than 400,000 journal references covering teratology and other aspects of developmental and reproductive toxicology.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://toxnet.nlm.nih.gov/newtoxnet/lactmed.htm" target="_blank">LactMed – Drugs and Lactation Database</a>
										</div>
										<p>The LactMed® database contains information on drugs and other chemicals to which breastfeeding mothers may be exposed.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="http://www.motherisk.org/" target="_blank">MotherRisk</a>
										</div>
										<p>The Motherisk Program at The Hospital for Sick Children is a teratogen information service providing up-to-date information about the risk and safety of medications and other exposures during pregnancy and breastfeeding. Research and education
											in the program is also an important ongoing activity.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://mothertobaby.org/" target="_blank">MotherToBaby</a>
										</div>
										<p>MotherToBaby is the public information service of the Organization of Teratology Information Specialists (OTIS), a professional scientific organization composed of experts engaged in assessing and evaluating risks to pregnancy and breastfeeding
											outcomes from medications and other exposures. This site is dedicated to providing evidence-based information to parents, health care professionals, and the general public about medications and other exposures during pregnancy and breastfeeding.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/obgyn/evolving-practice-perinatal-psychopharmacology-lessons-learned/" target="_blank">Evolving practice in perinatal psychopharmacology: Lessons learned</a>
										</div>
										<p>July 5, 2017 - The following post was first published in OB/GYN News. Please see our OB/GYN News archives here. Publish date: July 3, 2017   Over the last 2 decades, there has been a growing interest in establishing a rich […]</p>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
					        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseThree" aria-expanded="false">Resources for Addressing Non-Pharmacologic Treatment</a>
					      </h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="http://www.naspog.org/" target="_blank">North American Society for Psychosocial Obstetrics and Gynecology(NASPOG)</a>
										</div>
										<p>The aim of the Society is to foster scholarly scientific and clinical study of the biopsychosocial aspects of obstetric and gynecologic medicine. The aim is broadly defined to include the psychological, psychophysiological, public health, socio-cultural,
											ethical and other aspects of such functioning and behavior.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseSix" aria-expanded="false">Screening Tools</a>
								</h4>
							</div>
							<div id="collapseSix" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/obgyn/postpartum-depression-moving-toward-improved-screening-new-app/" target="_blank">Postpartum depression: Moving toward improved screening with a new app</a>
										</div>
										<p>January 19, 2018 - The following post was first published in OB/GYN News. Please see our OB/GYN News archives here. Publish date: November 22, 2017 Over the last several years, there’s been increasing interest and ultimately a growing number
											of mandates across […]</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/obgyn/perinatal-depression-screening-just-start/" target="_blank">Perinatal depression screening is just the start</a>
										</div>
										<p>March 23, 2017 - The following post was first published in OB/GYN News. Please see our OB/GYN News archives here. Publish date:  March 3, 2017 Over the last decade, appreciation of the prevalence of perinatal depression – depression during pregnancy
											and/or […]</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/obgyn/perinatal-depression-screening-new-recommendations-challenges/" target="_blank">Perinatal depression screening: New recommendations and challenges</a>
										</div>
										<p>March 23, 2016 - It was almost a year ago that the American College of Obstetricians and Gynecologists came out unequivocally in favor of universal screening for perinatal depression. In the revised policy statement from ACOG’s Committee on
											Obstetric […]
										</p>
									</div>
								</div>
							</div>
						</div> -->
					</div>
					<h3 class="learn-more__title learn-more__title--women">Women and Families</h3>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-orange">
							<div class="panel-heading">
								<h4 class="panel-title">
					        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseA" aria-expanded="false">Maternal Mental Health Resources</a>
					      </h4>
							</div>
							<div id="collapseA" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org" target="_blank">womensmentalhealth.org</a>
										</div>
										<p>Resources from MGH Center for Women’s Mental Health</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.nimh.nih.gov/health/topics/women-and-mental-health/index.shtml" target="_blank">Women and Mental Health</a>
										</div>
										<p>This site is an information resource from the National Institute of Mental Health.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.cdc.gov/pregnancy/meds/treatingfortwo/" target="_blank">Treating for Two</a>
										</div>
										<p>Treating for Two is a program launched by the Centers for Disease Control and Prevention, Treating for Two aims to improve the health of women and babies by working to identify the safest treatment options for the management of common conditions
											before and during pregnancy.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/wp-content/uploads/2018/05/mdd_guide.pdf" target="_blank">Major Depression During Conception and Pregnancy: A Guide for Patients and Families</a>
										</div>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://womensmentalhealth.org/wp-content/uploads/2018/05/postpartum_guide.pdf" target="_blank">Postpartum Depression: A Guide for Patients and Families</a>
										</div>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.acog.org/" target="_blank">The American College of Obstetricians and Gynecologists</a>
										</div>
										<p>ACOG provides a FAQs sheet on perinatal depression and offers a list of resources for patients and clinicians.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-orange">
							<div class="panel-heading">
								<h4 class="panel-title">
					        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseB" aria-expanded="false">Tools to Promote Wellness</a>
					      </h4>
							</div>
							<div id="collapseB" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="http://mindbodypregnancy.com/" target="_blank">Mind Body Pregnancy</a>
										</div>
										<p>Dr. Anna Glezer: A physician’s expert guidance on your mental health & emotional well-being during pregnancy, delivery, & postpartum. Articles, guides, & advice on mood, anxiety, mental illness, hormones, treatments, resources, & many others.</p>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://www.themotherhoodcenter.com/" target="_blank">The Motherhood Day Program</a>
										</div>
										<p>Motherhood Center of New York, New York, NY</p>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-orange">
							<div class="panel-heading">
								<h4 class="panel-title">
					        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseC" aria-expanded="false">Peer Support Resources</a>
					      </h4>
							</div>
							<div id="collapseC" class="panel-collapse collapsed collapse">
								<div class="panel-body">
									<div class="item">
										<div class="link">
											<a href="http://www.postpartum.net/locations/new-york/" target="_blank">Postpartum Support International</a>
										</div>
									</div>
									<div class="item">
										<div class="link">
											<a href="https://postpartumny.org" target="_blank">Postpartum Resource Center of NY</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="video-gallery--block">
		<h2 class="video-gallery--subtitle">Video Gallery</h2>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/294221615" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Accurate Diagnosis for the Primary Care Setting: Identification of Depression, Bipolar Disorder, Psychotic Disorders, and Anxiety Disorders</strong></p>
					<p class="video-title">Ruta Nonacs, MD, PhD</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/294633101" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Prevalence and Consequences of Maternal Mood and Anxiety Disorders</strong></p>
					<p class="video-title">Laura F. Petrillo, MD</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/294226215" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Substance Use Disorders (SUD) & Post Traumatic Stress Disorder (PTSD) in Women of Reproductive Age</strong></p>
					<p class="video-title">Edwin Raffi, MD, MPH</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/293627056" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Treatments for Pregnant and Postpartum Women</strong></p>
					<p class="video-title">Margaret Spinelli, MD</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/293626474" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Moving Beyond Cultural Competency: Cultural Complexities in Clinical Practice</strong></p>
					<p class="video-title">Nhi-Ha Trinh, MD, MPH</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 video-gallery--video">
					<iframe src="https://player.vimeo.com/video/294655481" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<p class="video-title"><strong>Screening for Maternal Mood and Anxiety Disorders</strong></p>
					<p class="video-title">Lisa S. Weinstock, MD</p>
				</div>
			</div>
		</div>
	</section>

	<div class="featured-faculty" id="faculty">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="faculty-info">
							<h2>Featured
	              Consultants</h2>
							<!-- <a href="https://ez372-aad517.pages.infusionsoft.net/" title="REGISTER NOW" class="btn register " target="_blank">FREE Registration</a>-->
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/ABaker2017.jpg" alt="Dr. Allison Baker, MD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Allison Baker, MD</h3>
										<p>Staff Psychiatrist, Perinatal and Reproductive Psychiatry Clinical Research Program<br> Massachusetts General Hospital</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="lee-cohen-md" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/LCohen2017-1.jpg" alt="Dr. Lee S. Cohen, MD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Lee Cohen, MD</h3>
										<p>Director, Ammon-Pinizzotto Center for Women’s Mental Health <br> Perinatal and Reproductive Psychiatry Clinical Research Program <br> Massachusetts General Hospital<br><br> Edmund and Carroll Carpenter Professor of Psychiatry<br> Harvard Medical
											School </p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/MFreeman2017-1.jpg" alt="Dr. Marlene P. Freeman, MD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Marlene Freeman, MD, MPH</h3>
										<p>Associate Director, Perinatal and Reproductive Psychiatry Clinical Research Program<br> Massachusetts General Hospital<br><br> Associate Professor of Psychiatry <br> Harvard Medical School</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/RNonacs2017.jpg" alt="Dr. Ruta Nonacs, MD, PhD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Ruta Nonacs, MD, PhD</h3>
										<p>Instructor, Psychiatry, Harvard Medical School <br><br> Staff Psychiatrist, <br> Perinatal and Reproductive Psychiatry Clinical Research Program; <br> Editor in Chief, womensmentalhealth.org, <br> The Ammon-Pinizzotto Center for Women’s Mental Health <br> Massachusetts General Hospital</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/LPetrillo2017.jpg" alt="Dr. Laura F. Petrillo, MD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Laura F. Petrillo, MD</h3>
										<p>Instructor, Psychiatry Harvard Medical School <br><br> Director of Training, The Ammon-Pinizzotto Center for Women’s  Mental Health, <br> Massachusetts  General Hospital</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/ERaffi2017.jpg" alt="Dr. Edwin Raffi, MD, MPH">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Edwin Raffi, MD, MPH</h3>
										<p>Instructor, Psychiatry, Harvard Medical School; <br><br> Staff Psychiatrist, The Ammon-Pinizzotto Center for Women’s Mental Health, <br> Massachusetts General Hospital; <br><br>Hope Clinic, Collaborative Care for Women with Dual Diagnosis</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 nopadding box-height">
					<div class="our-team cf">
						<ul>
							<li><img class="marlene-freeman-md-mph" src="http://project-teach.launchpaddev.com/wp-content/uploads/2018/10/BWang2017.jpg" alt="Dr. Betty Wang, MD">
								<div class="info-bio">
									<div class="bio-info-innner">
										<h3>Betty Wang, MD</h3>
										<p>On Staff, MGH Outpatient Child and Adolescent Psychiatry Department</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		var d = new Date();
		var theDate = d.getDate();
		var days= document.getElementById("october-days");
		for (var i = 0; i < theDate-1; i++) {
			days.children[i].classList.add("disabled");
		}

		days.children[theDate-1].classList.add("today");

		var october = document.getElementById("october");
		var november = document.getElementById("november");
		var december = document.getElementById("december");
		var january = document.getElementById("january");
		var february = document.getElementById("february");
		var march = document.getElementById("march");
		var april = document.getElementById("april");
		var may = document.getElementById("may");
		var june = document.getElementById("june");

		var octoberPrev = document.getElementById("october-prev");
		var octoberNext = document.getElementById("october-next");
		var novemberPrev = document.getElementById("november-prev");
		var novemberNext = document.getElementById("november-next");
		var decemberPrev = document.getElementById("december-prev");
		var decemberNext = document.getElementById("december-next");
		var januaryPrev = document.getElementById("january-prev");
		var januaryNext = document.getElementById("january-next");
		var februaryPrev = document.getElementById("february-prev");
		var februaryNext = document.getElementById("february-next");
		var marchPrev = document.getElementById("march-prev");
		var marchNext = document.getElementById("march-next");
		var aprilPrev = document.getElementById("april-prev");
		var aprilNext = document.getElementById("april-next");
		var mayPrev = document.getElementById("may-prev");
		var mayNext = document.getElementById("may-next");
		var junePrev = document.getElementById("june-prev");
		var juneNext = document.getElementById("june-next");

		octoberNext.addEventListener("click", function(){
			october.style.display = 'none';
			november.style.display = 'block';
		});
		novemberNext.addEventListener("click", function(){
			november.style.display = 'none';
			december.style.display = 'block';
		});
		decemberNext.addEventListener("click", function(){
			december.style.display = 'none';
			january.style.display = 'block';
		});
		januaryNext.addEventListener("click", function(){
			january.style.display = 'none';
			february.style.display = 'block';
		});
		februaryNext.addEventListener("click", function(){
			february.style.display = 'none';
			march.style.display = 'block';
		});
		marchNext.addEventListener("click", function(){
			march.style.display = 'none';
			april.style.display = 'block';
		});
		aprilNext.addEventListener("click", function(){
			april.style.display = 'none';
			may.style.display = 'block';
		});
		mayNext.addEventListener("click", function(){
			may.style.display = 'none';
			june.style.display = 'block';
		});

		novemberPrev.addEventListener("click", function(){
			november.style.display = 'none';
			october.style.display = 'block';
		});
		decemberPrev.addEventListener("click", function(){
			december.style.display = 'none';
			november.style.display = 'block';
		});
		januaryPrev.addEventListener("click", function(){
			january.style.display = 'none';
			december.style.display = 'block';
		});
		februaryPrev.addEventListener("click", function(){
			february.style.display = 'none';
			january.style.display = 'block';
		});
		marchPrev.addEventListener("click", function(){
			march.style.display = 'none';
			february.style.display = 'block';
		});
		aprilPrev.addEventListener("click", function(){
			april.style.display = 'none';
			march.style.display = 'block';
		});
		mayPrev.addEventListener("click", function(){
			may.style.display = 'none';
			april.style.display = 'block';
		});
		junePrev.addEventListener("click", function(){
			june.style.display = 'none';
			may.style.display = 'block';
		});

	</script>

	<?php require_once('footer.php'); ?>
