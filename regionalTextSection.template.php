      <!--reginal-text section ends-->
     <div class="home-comman-text">
        <div class="container">
            <div class="row">
                 <ol class="breadcrumb">
                    <li><a href="<?php echo get_link_by_slug("home"); ?>" title="HOME">HOME</a>
                        <?php if(is_page('intensive-training') | is_page('course-catalog')): ?>
                        <p class="bread-extra"><a href="<?php echo get_link_by_slug("training"); ?>" title="TRAINING">TRAINING</a></p>
                        <p><?php echo strtoupper(the_title('', '', false)); ?></p>
                        <?php elseif(is_author()): ?>
                          <?php $author = get_the_author_meta('display_name') ?>
                          <p><?php echo strtoupper($author); ?></p>
                        <?php elseif(is_archive('tribe_events') | is_page('team') | is_page('statewide-coordination-center') | is_page('regional-providers') | is_page('faqs')): ?>
                        <p class="bread-extra"><a href="/about/" title="ABOUT">ABOUT</a></p>
                        <p><?php echo strtoupper(the_title('', '', false)); ?></p>
                        <?php elseif(is_page('rating-scales') | is_page('prevention-science') | is_page('parent-and-family-page')): ?>
                        <p class="bread-extra"><a href="/resources/" title="RESOURCES">RESOURCES</a></p>
                        <p><?php echo strtoupper(the_title('', '', false)); ?></p> 
                        <?php else: ?>
                        <p><?php echo strtoupper(the_title('', '', false)); ?></p>
                        <?php endif; ?>
                    </li>
                  </ol>
            </div>
        </div>
     </div>
     <!--reginal-text section ends-->
