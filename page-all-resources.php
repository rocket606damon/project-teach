<?php /* Template Name: Resources All */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("resources",
		"/2017/09/regional-providers-bg.png",
		"Resources on Children's Mental Health",
    "Access videos and links on a variety of topics."); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<!--Start of Media Center Section-->
<section class="mainContent">
<div class="top-mediacenter">
  <div class="container">
  	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="pairagraph-txt">
      		<?php the_content(); ?>
    		</div>
    	</div>
  	</div>
  </div><!--End of Container-->
</div><!--End of topMediaCenter-->

<div id="tabs">
<div id="mediacenter-nav" class="mediacenter-nav">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="nav nav-tabs nav-justified">
          <li id="li-tabs-1" class="li-clicker"><a href="#tabs-1" class="tab-clicker" id="tab-clicker1"><i class="icon icon-videos"></i><span>VIDEOS</span></a></li>
          <li id="li-tabs-2" class="li-clicker"><a href="#tabs-2" class="tab-clicker" id="tab-clicker2"><i class="icon icon-newsletters"></i><span>NEWSLETTERS</span></a></li>
          <li id="li-tabs-3" class="li-clicker"><a href="#tabs-3" class="tab-clicker" id="tab-clicker3"><i class="icon icon-links"></i><span>RESOURCES</span></a></li>
          <li id="li-tabs-4" class="li-clicker"><a href="#tabs-4" class="tab-clicker" id="tab-clicker4"><i class="icon icon-publications"></i><span>PUBLICATIONS</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div><!--End of mediacenter-nav-->
<div id="tab-container" class="tab-container">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start of Tab 1-->
            <div id="tabs-1">
            <div class="tab-pane active" id="videos">
              <!-- <h2>Videos</h2> -->
              <div class="mediacenter-content">

                <?php $i = 1; while (have_rows('videos', 662)) : the_row(); ?>
                <?php
                  if(get_sub_field('video_type') == 'vimeo'):
                    $url = get_sub_field('vimeo');
                    $url = str_replace("http:", "", $url);
                    $url = str_replace("https:", "", $url);
                    $url = str_replace("www.", "", $url);
                    $url = str_replace("vimeo.com", "", $url);
                    $url = str_replace("/", "", $url);
                    $videoURL = '//player.vimeo.com/video/' . $url . '?title=0&amp;byline=0&amp;portrait=0';
                  else:
                    $url = get_sub_field('youtube');
                    $url = str_replace("http:", "", $url);
                    $url = str_replace("https:", "", $url);
                    $url = str_replace("www.", "", $url);
                    $url = str_replace("watch?v=", "", $url);
                    $url = str_replace("youtube.com", "", $url);
                    $url = str_replace("youtu.be", "", $url);
                    $url = str_replace("embed", "", $url);
                    $url = str_replace("/", "", $url);
                    $videoURL = '//www.youtube.com/embed/' . $url;
                  endif;
                ?>

                <div class="mvideo-box clearfix video video-<?php echo $i; ?>">
                  <div class="col-sm-5">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe src="<?php echo str_replace(array('https:','http:'), '', $videoURL); ?>" allowfullscreen></iframe>
                    </div>
                  </div>
                  <div class="col-sm-7">
                    <h4><?php the_sub_field('video_title'); ?></h4>
                    <?php if(get_sub_field('video_description')): ?><p class="video-description"><?php the_sub_field('video_description'); ?></p><?php endif; ?>
                  </div>
                </div>

                <?php $i++; endwhile; ?>

                <!-- <div class="more-vids-link">
                  <a id="media-vid-btn" class="btn btn-primary btn-block media-vid-btn" href="#">Load More Videos</a>
                </div> -->
              </div>
            </div>
            </div><!--End of tab 1-->

            <!--Start of Tab 2-->
            <div id="tabs-2">
            <div class="tab-pane active" id="newsletters">
              <h2>Newsletters</h2>
              <div class="accordian">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php $n = 1; while (have_rows('newsletters', 662)) : the_row(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title"><a data-toggle="collapse"<?php if($n != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $n; ?>"><?php the_sub_field('title'); ?></a></h3>
                    </div>
                    <div id="collapse<?php echo $n; ?>" class="panel-collapse collapse <?php if($n != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                        <div class="panel-body">
                          <?php the_sub_field('content'); ?>
                        </div>
                    </div>
                </div>
              <?php $n++; endwhile; ?>
                </div>
              </div>

            </div>
            </div>
            <!--End of Tab 2-->

            <!--Start of Tab 3-->
            <div id="tabs-3">
            <div class="tab-pane active" id="links">
              <!-- <h2>Links</h2> -->
              <p class="link-top"><?php the_field('link_top', 662); ?></p>
              <div class="mediacenter-content">
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <?php $count = ''; $i = 1; while (have_rows('links', 662)) : the_row(); ?>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" data-target="#link<?php echo $i; ?>" aria-expanded="false"><?php the_sub_field('section_title'); ?></a>
                      </h4>
                    </div>
                    <div id="link<?php echo $i; ?>" class="panel-collapse collapsed collapse">
                      <div class="panel-body">
                        <?php $k = 0; while (have_rows('links_list', 662)) : the_row(); ?>
                        <div class="item item-<?php echo $k; ?>">
                          <div class="link">
                            <a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a>
                          </div>
                          <p class="standard"><?php the_sub_field('text'); ?></p></p>
                        </div>
                        <?php $k++; endwhile; ?>
                      </div>
                    </div>
                  </div>
                  <?php $i++; endwhile; ?>

                </div>
                </div>
                </div>
              </div>
            </div>
            </div>
            <!--End of Tab 3-->

            <!--Start of Tab 4-->
            <div id="tabs-4">
            <div class="tab-pane active" id="publications">

            </div>
            </div>
            <!--End of Tab 4-->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!--End of Tabs-->
</section>

</div>
 <?php require_once('footer.php'); ?>