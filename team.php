<?php /* Template Name: Team Template */ ?>
<?php require_once('header.php'); ?>
<?php renderBanner("about_block",
    "/2017/09/about-bg.png",
    "Better Health.<br> Brighter Future.",
    "Learn more about the individuals who are making for a healthy New York State"); ?>
<?php require(dirname(__FILE__)."/regionalTextSection.template.php"); ?>
<?php
while ( have_posts() ) : the_post(); ?>
  <div class="team-intro">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 team-head">
            <p class="standard">For love to flourish and endure, our living framework should typically function in a satisfactory manner. When romantic love thrives, it can enhance the flourishing of our life.</p>
            <ul class="nav nav-pills nav-justified">
              <li class="active"><a data-toggle="pill" data-target="#region_1">Region 1 and 3</a></li>
              <li><a data-toggle="pill" data-target="#region_2">Region 2</a></li>
            </ul>
          </div>

          <div class="tab-content">

          	<section id="region_1" class="tab-pane fade in active">
            <?php
              if( have_rows('content') ):
              while ( have_rows('content') ) : the_row();
              if( get_row_layout() == 'team_list_region_1' ):
              ?>
                  <div class="row">
                    <div class="col-sm-12 team-list">
                    <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
                	<?php if( have_rows('team_members') ): ?>
                    <table class="table table-bordered table-striped table-responsive">
                      <tbody>
                      <?php 
                    while ( have_rows('team_members') ) : the_row(); 
                    $slug =  custom_slug(get_sub_field('name'));
                  ?>
                      <tr>
                        <?php if(get_sub_field('title')): ?><td<?php echo $columnClass; ?>><?php the_sub_field('title'); ?></td><?php endif; ?>
                        <?php if(get_sub_field('bio')): ?>
                        <td<?php echo $columnClass; ?>><a href="#" data-toggle="modal" data-target="#<?php echo $slug; ?>"><?php the_sub_field('name'); ?></a></td>
                        <?php else: ?>
                        <td<?php echo $columnClass; ?>><?php the_sub_field('name'); ?></td>
                        <?php endif; ?>
                        <td<?php echo $columnClass; ?>><?php the_sub_field('phone'); ?></td>
                        <td<?php echo $columnClass; ?>><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></td>
                    	</tr>
                      <?php endwhile; ?>
                      </tbody>
                    </table>
                <?php endif; ?>
                    </div>
                  </div>
                  <?php  if( have_rows('team_members') ): ?>
                  <div class="row">
                    <div class="col-sm-12">
                  <?php 
                        while ( have_rows('team_members') ) : the_row(); 
                        $slug =  custom_slug(get_sub_field('name'));
                      ?>
                      <div class="modal team-bio" id="<?php echo $slug; ?>" role="dialog" aria-labelledby="<?php echo $slug; ?>Label">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="<?php echo $slug; ?>Label"><h3><?php the_sub_field('name'); ?></h3></h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <?php if(get_sub_field('image')): ?>
                                <div class="col-sm-3 image">
                                    <img class="img-rounded img-responsive" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>">
                                </div>
                                <div class="col-sm-9">
                                    <?php the_sub_field('profile'); ?>
                                </div>
                                <?php else: ?>
                                 <div class="col-sm-12">
                                 <?php the_sub_field('profile'); ?>
                                 </div>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endwhile; endif; ?>
                  </div>
                  </div>
              <?php endif; endwhile; endif; ?>
          </section>

          <section id="region_2" class="tab-pane fade in">
            <?php
              if( have_rows('content') ):
              while ( have_rows('content') ) : the_row();
              if( get_row_layout() == 'team_list_region_2' ):
              ?>
                  <div class="row">
                    <div class="col-sm-12 team-list">
                    <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
                	<?php if( have_rows('team_members') ): ?>
                    <table class="table table-bordered table-striped table-responsive">
                      <tbody>
                      <?php 
                    while ( have_rows('team_members') ) : the_row(); 
                    $slug =  custom_slug(get_sub_field('name'));
                  ?>
                      <tr>
                        <?php if(get_sub_field('title')): ?><td<?php echo $columnClass; ?>><?php the_sub_field('title'); ?></td><?php endif; ?>
                        <?php if(get_sub_field('bio')): ?>
                        <td<?php echo $columnClass; ?>><a href="#" data-toggle="modal" data-target="#<?php echo $slug; ?>"><?php the_sub_field('name'); ?></a></td>
                        <?php else: ?>
                        <td<?php echo $columnClass; ?>><?php the_sub_field('name'); ?></td>
                        <?php endif; ?>
                        <?php if(get_sub_field('phone')): ?><td<?php echo $columnClass; ?>><?php the_sub_field('phone'); ?></td><?php endif; ?>
                        <?php if(get_sub_field('email')): ?><td<?php echo $columnClass; ?>><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></td><?php endif; ?>
                    	</tr>
                      <?php endwhile; ?>
                      </tbody>
                    </table>
                <?php endif; ?>
                    </div>
                  </div>
                  <?php  if( have_rows('team_members') ): ?>
                  <div class="row">
                    <div class="col-sm-12">
                  <?php 
                        while ( have_rows('team_members') ) : the_row(); 
                        $slug =  custom_slug(get_sub_field('name'));
                      ?>
                      <div class="modal team-bio" id="<?php echo $slug; ?>" role="dialog" aria-labelledby="<?php echo $slug; ?>Label">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="<?php echo $slug; ?>Label"><h3><?php the_sub_field('name'); ?></h3></h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <?php if(get_sub_field('image')): ?>
                                <div class="col-sm-3 image">
                                    <img class="img-rounded img-responsive" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>">
                                </div>
                                <div class="col-sm-9">
                                    <?php the_sub_field('profile'); ?>
                                </div>
                                <?php else: ?>
                                 <div class="col-sm-12">
                                 <?php the_sub_field('profile'); ?>
                                 </div>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endwhile; endif; ?>
                  </div>
                  </div>
              <?php endif; endwhile; endif; ?>
          </section>

          </div>

        </div>
      </div>
    </div> 
<?php endwhile; ?>
</div>
<?php require_once('footer.php'); ?>